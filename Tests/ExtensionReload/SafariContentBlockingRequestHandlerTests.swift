// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitSafari
import XCTest

class SafariContentBlockingRequestHandlerTests: XCTestCase {
    let cbIdentifier = "com.hostapp.contentBlocker"

    func test_loadFilterListItem_noFilterListAtURL_throwsError() {
        let containerURL = filterListContainerTestURL()
        let sut = makeSUT(filterListContainerURL: containerURL)

        expectOperation(curry(sut.getFilterListItem)(cbIdentifier),
                        toCompleteWith: .anyFailureWithCode(.missingActiveFilterList),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))
    }

    func test_loadFilterListItem_filterListPresentAtURL_loadsItems() throws {
        let containerURL = filterListContainerTestURL()
        let sut = makeSUT(filterListContainerURL: containerURL)

        let rawFilteList = anyRawFiterList()
        let filterListURL = containerURL.appendingPathComponent("\(cbIdentifier).json")
        try XCTUnwrap(rawFilteList.write(to: filterListURL,
                                         atomically: true,
                                         encoding: .utf8))

        let exp = expectation(description: #function)
        sut.getFilterListItem(forContentBlocker: cbIdentifier) { result in
            switch result {
            case .failure:
                XCTFail("Expected get to succeed")
                exp.fulfill()
            case let .success(item):
                self.assertThat(itemProvider: item, containsRules: rawFilteList, expectationToFulfill: exp)
            }
        }
        wait(for: [exp], timeout: 1.0)
        try XCTUnwrap(FileManager.default.removeItem(at: filterListURL))
    }

    func test_completeRequestForCB_throwsErrorOnFailedToGetItem() {
        let sut = makeSUT(filterListContainerURL: filterListContainerTestURL())
        let mockContext = MockExtensionContext()

        expectOperation(curry(sut.completeExtensionRequest)(mockContext, cbIdentifier),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==))
    }

    func test_completeRequestForCB_cancelsRequestIfFailedToCreateItem() {
        let sut = makeSUT(filterListContainerURL: filterListContainerTestURL())
        let mockContext = MockExtensionContext()

        sut.completeExtensionRequest(inContext: mockContext,
                                     forContentBlocker: cbIdentifier) { _ in }

        XCTAssertEqual(mockContext.cancelRequestsCount, 1)
    }

    func test_completeRequestForCB_completesCorrectlyTheRequest() throws {
        let containerURL = filterListContainerTestURL()
        let sut = makeSUT(filterListContainerURL: containerURL)
        let mockContext = MockExtensionContext()

        let rawFilteList = anyRawFiterList()
        let filterListURL = containerURL.appendingPathComponent("\(cbIdentifier).json")
        try XCTUnwrap(rawFilteList.write(to: filterListURL,
                                         atomically: true,
                                         encoding: .utf8))
        let exp = expectation(description: #function)

        var capturedResult: Result<Bool, Error>?

        sut.completeExtensionRequest(inContext: mockContext,
                                     forContentBlocker: cbIdentifier) { result in
                                        capturedResult = result
                                        exp.fulfill()
        }
        mockContext.respondToCompleteRequest(with: true)
        wait(for: [exp], timeout: 1.0)

        let assertExpectation = expectation(description: "Wait for assertion")
        switch capturedResult {
        case let .success(state):
            XCTAssertTrue(state)
            let requestItem = try XCTUnwrap(mockContext.completeRequests.first?.items?.first, "Expected request item ot not be nil")
            let extensionItem = try XCTUnwrap(requestItem as? NSExtensionItem, "Expected request item to be NSExtensionItem, instead found \(requestItem)")
            let extensionItemAttachment = try XCTUnwrap(extensionItem.attachments?.first, "Expected extension item attachment to not be nil")
            self.assertThat(itemProvider: extensionItemAttachment,
                            containsRules: rawFilteList,
                            expectationToFulfill: assertExpectation)
        case .failure:
            XCTFail("Expected complete request to succeed")
        case .none:
            XCTFail("Expected to emit a result")
        }

        wait(for: [assertExpectation], timeout: 1.0)

        try XCTUnwrap(FileManager.default.removeItem(at: filterListURL))
    }

    // MARK: - Helpers

    private func makeSUT(filterListContainerURL: URL,
                         file: StaticString = #file,
                         line: UInt = #line) -> (SafariContentBlockingRequestHandler) {
        let sut = SafariContentBlockingRequestHandler(filterListContainerURL: filterListContainerURL)

        trackForMemoryLeaks(sut, file: file, line: line)

        return sut
    }

    private func assertThat(itemProvider: NSItemProvider,
                            containsRules rules: String,
                            expectationToFulfill: XCTestExpectation? = nil,
                            file: StaticString = #file,
                            line: UInt = #line) {
        itemProvider.loadItem(forTypeIdentifier: "public.json") { data, error in

            XCTAssertNotNil(data, "Expected item data to not be nil", file: file, line: line)
            XCTAssertNil(error, "Expeted to not fail with item data", file: file, line: line)

            guard let data = data as? Data else {
                XCTFail("Expected item data to not be nil", file: file, line: line)
                expectationToFulfill?.fulfill()
                return
            }

            let decodedRules = String(data: data, encoding: .ascii)

            XCTAssertNotNil(decodedRules,
                            "Expeted rules to be saved in correct format",
                            file: file,
                            line: line)
            XCTAssertEqual(rules,
                           decodedRules,
                           "Expected item rules to be \(rules), found \(String(describing: decodedRules)) instead",
                           file: file,
                           line: line)
            expectationToFulfill?.fulfill()
        }
    }

    // swiftlint:disable discouraged_optional_collection
    private class MockExtensionContext: NSExtensionContext {

        struct CompleteRequest {
            let items: [Any]?
            let completion: ((Bool) -> Void)?
        }

        private(set) var completeRequests = [CompleteRequest]()
        private(set) var cancelRequestsCount = 0

        override func completeRequest(returningItems items: [Any]?, completionHandler: ((Bool) -> Void)? = nil) {
            completeRequests.append(CompleteRequest(items: items, completion: completionHandler))
        }

        override func cancelRequest(withError error: Error) {
            cancelRequestsCount += 1
        }
        func respondToCompleteRequest(at idx: Int = 0, with status: Bool) {
            completeRequests[idx].completion?(status)
        }
    }

    func filterListContainerTestURL() -> URL {
        FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
    }
}
