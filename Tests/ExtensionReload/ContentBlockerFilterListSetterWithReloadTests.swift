// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import XCTest
@testable import ABPKitSafari

class ContentBlockerFilterListSetterWithReloadTests: XCTestCase {

    func test_setFilterList_triggersSetFilterList() {
        let (sut, decoratee, _) = makeSUT()

        let source = anyFilterListSource()
        let cbId = anyContentBlockerId()

        sut.setFilterList(forSource: source, toContentBlocker: cbId, completion: { _ in })

        XCTAssertEqual(decoratee.requestedParams, [.init(source: source, cbId: cbId)])
    }

    func test_setFilterList_failedToSetFilerList_noReloadIsTriggered() {
        let (sut, decoratee, extensionReload) = makeSUT()

        sut.setFilterList(forSource: anyFilterListSource(),
                          toContentBlocker: anyContentBlockerId(),
                          completion: { _ in })
        decoratee.completeOperation(with: .anyFailure)

        XCTAssertTrue(extensionReload.requestedParams.isEmpty,
                      "Expected to not trigger a reload if filter list set failed")
    }

    func test_setFilterList_filterListSetSucceeded_triggersExtensionReload() {
        let (sut, decoratee, extensionReload) = makeSUT()

        let cbIdentifier = anyContentBlockerId()

        sut.setFilterList(forSource: anyFilterListSource(),
                          toContentBlocker: cbIdentifier,
                          completion: { _ in })
        decoratee.completeOperation(with: .success)

        XCTAssertEqual(extensionReload.requestedParams, [cbIdentifier])
    }

    func test_setFilterList_reloadFailed_throwsError() {
        let (sut, decoratee, extensionReload) = makeSUT()

        expectOperation(curry(sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .success)
                            extensionReload.completeOperation(with: .anyFailure)
        })
    }

    func test_setRules_reloadSucceeds_completesWithSuccess() {
        let (sut, decoratee, extensionReload) = makeSUT()

        expectOperation(curry(sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .success)
                            extensionReload.completeOperation(with: .success)
        })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> (FilterListSetter, FilterListSetterMock, ExtensionReloaderMock) {
        let extensionReloader = ExtensionReloaderMock()
        let rulesSetter = FilterListSetterMock()
        let sut = FilterListSetterWithExtensionReload(decoratee: rulesSetter,
                                                           extensionReloader: extensionReloader)

        trackForMemoryLeaks(sut, file: file, line: line)

        return (sut, rulesSetter, extensionReloader)
    }
}
