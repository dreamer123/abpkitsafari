/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

import XCTest
@testable import ABPKitSafari

class ContentBlockersReloaderTests: XCTestCase {

    func test_init_noCommands() {
        let (_, activeSourcesLoader, filterListSetter) = makeSUT()

        XCTAssertTrue(activeSourcesLoader.requests.isEmpty)
        XCTAssertTrue(filterListSetter.requestedParams.isEmpty)
    }

    func test_reloadContentBlockers_requestsActiveSources() {
        let (sut, activeSourcesLoader, _) = makeSUT()

        sut.reloadContentBlockers { _ in }
        XCTAssertTrue(activeSourcesLoader.requests.count == 1)
    }

    func test_loadActiveSourcesFailed_doesNotTriggerFilterListSetting() {
        let (sut, activeSourcesLoader, filterListSetter) = makeSUT()

        sut.reloadContentBlockers { _ in }
        activeSourcesLoader.completeOperation(with: .anyFailure)
        XCTAssertTrue(filterListSetter.requestedParams.isEmpty)
    }

    func test_noActiveSources_doesNotTriggerFilterListSetting() {
        let (sut, activeSourcesLoader, filterListSetter) = makeSUT()

        sut.reloadContentBlockers { _ in }
        activeSourcesLoader.completeOperation(with: .success([]))
        XCTAssertTrue(filterListSetter.requestedParams.isEmpty)
    }

    func test_activeSourcesPresent_triggersFilterListSetting() {
        let (sut, activeSourcesLoader, filterListSetter) = makeSUT()

        sut.reloadContentBlockers { _ in }

        let activeSource1 = ContentBlockerActiveSource(source: .remote(.abp(.withAA(.easyList))),
                                                       cbId: "id1")
        let activeSource2 = ContentBlockerActiveSource(source: .bundled(.abp(.withAA(.easyList))),
                                                       cbId: "id2")
        activeSourcesLoader.completeOperation(with: .success([activeSource1, activeSource2]))

        let expectedRequests = [FilterListSetRequest(source: activeSource1.source,
                                                     cbId: activeSource1.cbId),
                                FilterListSetRequest(source: activeSource2.source,
                                                     cbId: activeSource2.cbId)]
        XCTAssertEqual(filterListSetter.requestedParams, expectedRequests)
    }

    func test_reload_failedToLoadActiveSources_emittsFinishEventWithError() {
        let (sut, activeSourcesLoader, _) = makeSUT()

        var collectedEvents = [ContentBlockerReloadEvent]()
        sut.reloadContentBlockers { collectedEvents.append($0) }
        activeSourcesLoader.completeOperation(with: .anyFailure)

        XCTAssertTrue(collectedEvents.isEqual(to: [.finishedReloading(result: .anyFailure)], failureType: NSError.self),
                      "Expected to finish loading with failure, instead emitted events: \(collectedEvents)")
    }

    func test_reload_noActiveSources_emittsNoActiveSourcesError() {
        let (sut, activeSourcesLoader, _) = makeSUT()

        var collectedEvents = [ContentBlockerReloadEvent]()
        sut.reloadContentBlockers { collectedEvents.append($0) }
        activeSourcesLoader.completeOperation(with: .success([]))

        let expectedEvents: [ContentBlockerReloadEvent] = [.finishedReloading(result: .failure(ContentBlockersReloader.NoActiveSourcesError()))]
        XCTAssertTrue(collectedEvents.isEqual(to: expectedEvents,
                                              failureType: ContentBlockersReloader.NoActiveSourcesError.self),
                      "Expected to finish loading with failure, instead emitted events: \(collectedEvents)")
    }

    // swiftlint:disable function_body_length
    func test_reload_updatedContentBlockers_completesWithResults() {
        // - Arrange
        let (sut, activeSourcesLoader, filterListSetter) = makeSUT()

        let activeSource1 = ContentBlockerActiveSource(source: .remote(.abp(.withAA(.easyList))),
                                                       cbId: "id1")
        let activeSource2 = ContentBlockerActiveSource(source: .bundled(.abp(.withAA(.easyList))),
                                                       cbId: "id2")
        let activeSource3 = ContentBlockerActiveSource(source: .remote(.abp(.withoutAA(.easyList))),
                                                       cbId: "id3")
        let activeSource4 = ContentBlockerActiveSource(source: .bundled(.abp(.withoutAA(.easyList))),
                                                       cbId: "id4")
        let activeSources = [activeSource1, activeSource2, activeSource3, activeSource4]

        // - Act

        let reloadExpectation = expectation(description: "Wait for reload")
             reloadExpectation.expectedFulfillmentCount = 2
        var collectedEvents = [ContentBlockerReloadEvent]()
        sut.reloadContentBlockers { event in
            collectedEvents.append(event)
            reloadExpectation.fulfill()
        }

        activeSourcesLoader.completeOperation(with: .success(activeSources))

        // Simulate also the dispatch from different threads
        DispatchQueue.global().async {
            filterListSetter.completeOperation(with: .success, at: 0)
        }
        DispatchQueue.global().async {
            filterListSetter.completeOperation(with: .anyFailure, at: 1)
        }
        DispatchQueue.global().async {
            filterListSetter.completeOperation(with: .anyFailure, at: 2)
        }
        DispatchQueue.global().async {
            filterListSetter.completeOperation(with: .anyFailure, at: 3)
        }
        wait(for: [reloadExpectation], timeout: 1.0)

        let expectedCompleteResults = [ContentBlockerReloadResult(activeSource: activeSource1,
                                                                  result: .success),
                                       ContentBlockerReloadResult(activeSource: activeSource2,
                                                                  result: .anyFailure),
                                       ContentBlockerReloadResult(activeSource: activeSource3,
                                                                  result: .anyFailure),
                                       ContentBlockerReloadResult(activeSource: activeSource4,
                                                                  result: .anyFailure)]

        if case let .startedReloading(collectedActiveSources) = collectedEvents[0] {
            XCTAssertTrue(collectedActiveSources.count == activeSources.count)
            activeSources.forEach {
                if !collectedActiveSources.contains($0) {
                    XCTFail("Expected to collect event \($0)")
                }
            }
        } else {
            XCTFail("Expected first event to be started reloading, instead it is \(collectedEvents[0])")
        }

        if case let .finishedReloading(collectedResult) = collectedEvents[1] {
            guard let collectedResults = try? collectedResult.get() else {
                return XCTFail("Expected to finish reloading with success")
            }

            XCTAssertTrue(collectedResults.count == expectedCompleteResults.count)
            collectedResults.forEach {
                if !expectedCompleteResults.contains($0) {
                    XCTFail("Expected to collect result \($0)")
                }
            }
        } else {
            XCTFail("Expected last event to be finished reloading, instead it is \(collectedEvents[1])")
        }
    }

    // MARK: - Helpers

    func makeSUT() -> (ContentBlockersReloader, ActiveSourcesLoaderMock, FilterListSetterMock) {
        let activeSourcesLoader = ActiveSourcesLoaderMock()
        let filterListSetter = FilterListSetterMock()
        let sut = ContentBlockersReloader(activeSourcesLoader: activeSourcesLoader,
                                          filterListSetter: filterListSetter)
        trackForMemoryLeaks(sut)
        return (sut, activeSourcesLoader, filterListSetter)
    }
}

extension ContentBlockerReloadEvent {

    var isReloading: Bool {
        guard case .startedReloading = self else {
            return false
        }
        return true
    }

    func isEqual<F: Equatable>(to other: ContentBlockerReloadEvent, failureType: F.Type) -> Bool {
        switch (self, other) {
        case let (.startedReloading(lhsSources), .startedReloading(activeSources: rhsSources)):
            return lhsSources == rhsSources
        case let (.finishedReloading(lhsResult), .finishedReloading(result: rhsResult)):
            return resultIsEqual(==, castedErrorIsEqual(cast: failureType))(lhsResult, rhsResult)
        default:
            return false
        }
    }
}

extension Array where Element == ContentBlockerReloadEvent {
    func isEqual<F: Equatable>(to other: Array, failureType: F.Type) -> Bool {
        zip(self, other).allSatisfy { lhs, rhs in
            return lhs.isEqual(to: rhs, failureType: failureType)
        }
    }
}
