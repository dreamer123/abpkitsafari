// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation
@testable import ABPKitSafari

func anyNSError(_ code: Int = 3) -> Error {
    NSError(domain: "test", code: code)
}

func anyFilterListSource() -> FilterListSource {
    .remote(anyRemoteFilterListSource())
}

func anyRemoteFilterListSource() -> FilterListType {
    .abp(.withAA(.easyList))
}

func anyNonRemoteFilterListSource() -> FilterListSource {
    .bundled(.abp(.withAA(.easyList)))
}

func anyContentBlockerId() -> ContentBlockerIdentifier {
    "com.sample.blockerid"
}

func anyContentBlockerActiveSource(source: FilterListSource = anyFilterListSource(),
                                   cbId: ContentBlockerIdentifier = anyContentBlockerId()) -> ContentBlockerActiveSource {
    .init(source: source, cbId: cbId)
}

func anyFilterList() -> FilterList {
    .init(content: anyRawFiterList())
}

func anyRawFiterList() -> String {
    "[{trigger: {url-filter: ^https?://([^/:]*\\.)?community\\.adlandpro\\.com[/:], url-filter-is-case-sensitive: true}, action: {type: css-display-none, selector: [id=AdContent], [id=ctl00_slider]}}]"
}
