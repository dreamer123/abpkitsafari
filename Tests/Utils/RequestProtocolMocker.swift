// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation
import ABPKitCore
@testable import ABPKitSafari

final class RequestProtocolMocker<Param: Equatable, LoadedValue> {
    struct Requests {
        let param: Param
        let completion: (LoadedValue) -> Void
    }

    var requestedParams: [Param] {
        return requests.map { $0.param }
    }

    var requests = [Requests]()

    func completeOperation(with result: LoadedValue, at idx: Int = 0) {
        requests[idx].completion(result)
    }
}

final class VoidRequestProtocolMocker<LoadedValue> {
    struct Requests {
        let completion: (LoadedValue) -> Void
    }

    var requests = [Requests]()

    func completeOperation(with result: LoadedValue, at idx: Int = 0) {
        requests[idx].completion(result)
    }
}

// MARK: - FilterListLoader Mock -

typealias FilterListLoaderMock = RequestProtocolMocker<FilterListSource, FilterListLoader.LoadResult>

extension RequestProtocolMocker: FilterListLoader where
          Param == FilterListSource, LoadedValue == FilterListLoader.LoadResult {
    func loadFilterList(forSource source: FilterListSource,
                        completion: @escaping FilterListLoader.LoadCompletion) {
        requests.append(Requests(param: source, completion: completion))
    }
}

typealias RemoteSourceFilterListLoaderMock = RequestProtocolMocker<FilterListType, RemoteSourceFilterListLoader.LoadResult>

extension RequestProtocolMocker: RemoteSourceFilterListLoader where
          Param == FilterListType, LoadedValue == RemoteSourceFilterListLoader.LoadResult {
    func loadFilterList(forSource source: FilterListType,
                        completion: @escaping RemoteSourceFilterListLoader.LoadCompletion) {
        requests.append(Requests(param: source, completion: completion))
    }
}

// MARK: - FilterListSaver Mock -

typealias ActiveFilterListSaverMock = RequestProtocolMocker<ActiveFilterList, ActiveFilterListSaver.SaveResult>

extension RequestProtocolMocker: ActiveFilterListSaver where Param == ActiveFilterList, LoadedValue == ActiveFilterListSaver.SaveResult {
    func saveActiveFilterList(_ filterList: ActiveFilterList, completion: @escaping SaveCompletion) {
        requests.append(.init(param: filterList, completion: completion))
    }
}

// MARK: - FilterListSetter Mock -

struct FilterListSetRequest: Equatable {
    let source: FilterListSource
    let cbId: ContentBlockerIdentifier
}

typealias FilterListSetterMock = RequestProtocolMocker<FilterListSetRequest, FilterListSetter.SetResult>
extension RequestProtocolMocker: FilterListSetter where Param == FilterListSetRequest, LoadedValue == FilterListSetter.SetResult {
    func setFilterList(forSource source: FilterListSource, toContentBlocker cbIdentifier: ContentBlockerIdentifier, completion: @escaping SetCompletion) {
        requests.append(.init(param: .init(source: source, cbId: cbIdentifier),
                              completion: completion))
    }
}

// MARK: - ContentBlocker reload mock -

typealias ExtensionReloaderMock = RequestProtocolMocker<ContentBlockerIdentifier, ExtensionReloader.ReloadResult>
extension RequestProtocolMocker: ExtensionReloader where Param == ContentBlockerIdentifier, LoadedValue == ExtensionReloader.ReloadResult {
    func reloadContentBlocker(forId cbId: ContentBlockerIdentifier, completion: @escaping ReloadCompletion) {
        requests.append(.init(param: cbId, completion: completion))
    }
}

// MARK: - ActiveSourceSaver mock -

typealias ActiveSourceSaverMock = RequestProtocolMocker<ContentBlockerActiveSource, ActiveSourceSaver.SaveResult>
extension RequestProtocolMocker: ActiveSourceSaver where Param == ContentBlockerActiveSource, LoadedValue == ActiveSourceSaver.SaveResult {
    func saveActiveSource(_ activeSource: ContentBlockerActiveSource, completion: @escaping SaveCompletion) {
        requests.append(.init(param: activeSource, completion: completion))
    }
}

typealias ActiveSourcesLoaderMock = VoidRequestProtocolMocker<ActiveSourcesLoader.LoadResult>
extension VoidRequestProtocolMocker: ActiveSourcesLoader where LoadedValue == ActiveSourcesLoader.LoadResult {
    func loadActiveSources(completion: @escaping ActiveSourcesLoader.LoadCompletion) {
        requests.append(.init(completion: completion))
    }
}
