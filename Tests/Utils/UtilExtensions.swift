/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

@testable import ABPKitSafari

extension Result where Failure == Error {
    static var anyFailure: Result {
        .failure(anyNSError())
    }

    static func anyFailureWithCode(_ code: ABPKitSafariError.ABPKitSafariErrorCode) -> Result {
        .failure(anyNSError(code.rawValue))
    }

    static func anyFailureWithCode(_ code: Int) -> Result {
        .failure(anyNSError(code))
    }
}

extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}

extension ContentBlockerReloadResult: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(activeSource.cbId.hashValue)
    }
}

extension ContentBlockerReloadResult: Equatable {
    public static func == (lhs: ContentBlockerReloadResult, rhs: ContentBlockerReloadResult) -> Bool {
        guard lhs.activeSource == rhs.activeSource else {
            return false
        }

        switch (lhs.result, rhs.result) {
        case (.success, .success), (.failure, .failure):
            return true
        default:
            return false
        }
    }
}
