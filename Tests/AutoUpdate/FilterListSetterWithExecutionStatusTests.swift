/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

import XCTest
@testable import ABPKitSafari

final class FilterListSetterWithExecutionStatusTests: XCTestCase {

    func test_init_isNotExecutingAndNoCommands() {
        let (sut, decorateee) = makeSUT()

        XCTAssertFalse(sut.isExecuting, "Expected to not be executing on initialization")
        XCTAssertEqual(decorateee.requestedParams, [], "Expected no commands on decoratee")
    }

    func test_set_isExecutingAndSendsTheCommand() {
        let (sut, decorateee) = makeSUT()
        let source = anyFilterListSource()
        let cbId = anyContentBlockerId()

        sut.setFilterList(forSource: source, toContentBlocker: cbId, completion: { _ in })

        XCTAssertTrue(sut.isExecuting, "Expected to be executing on filter list set")
        XCTAssertEqual(decorateee.requestedParams, [.init(source: source, cbId: cbId)])
    }

    func test_set_decorateeCompletes_isNotExecutingAndForwardsTheResult() {
        let (sut, decorateee) = makeSUT()
        let source = anyFilterListSource()
        let cbId = anyContentBlockerId()

        expectOperation(curry(sut.setFilterList)(source, cbId),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==),
                        when: {
            decorateee.completeOperation(with: .success)
        })

        XCTAssertFalse(sut.isExecuting, "Expected to not be executing once decoratee completed")
    }

    func test_set_multipleParallelFilterListSet_worksAccordingly() {
        let (sut, decorateee) = makeSUT()
        let source = anyFilterListSource()
        let cbId = anyContentBlockerId()

        let expectation0 = expectation(description: "Wait for set 0")
        let expectation1 = expectation(description: "Wait for set 1")
        let expectation2 = expectation(description: "Wait for set 2")

        sut.setFilterList(forSource: source, toContentBlocker: cbId, completion: { _ in expectation0.fulfill() })
        sut.setFilterList(forSource: source, toContentBlocker: cbId, completion: { _ in expectation1.fulfill() })
        sut.setFilterList(forSource: source, toContentBlocker: cbId, completion: { _ in expectation2.fulfill() })

        decorateee.completeOperation(with: .success, at: 0)
        wait(for: [expectation0], timeout: 1.0)
        XCTAssertTrue(sut.isExecuting, "Expecting filter list set to be executing after first decoratte request completed")

        decorateee.completeOperation(with: .anyFailure, at: 2)
        wait(for: [expectation2], timeout: 1.0)
        XCTAssertTrue(sut.isExecuting, "Expecting filter list set to be executing after second decoratte request completed")

        decorateee.completeOperation(with: .success, at: 1)
        wait(for: [expectation1], timeout: 1.0)
        XCTAssertFalse(sut.isExecuting, "Expecting filter list set to not be executing after last decoratee request completed")
    }

    func test_set_decorateeDispatchesFromDifferentThreads_worksAccordingly() {
        let (sut, decorateee) = makeSUT()
        let source = anyFilterListSource()
        let cbId = anyContentBlockerId()

        let setExpectation = expectation(description: "Wait for set")
        setExpectation.expectedFulfillmentCount = 3

        sut.setFilterList(forSource: source, toContentBlocker: cbId, completion: { _ in setExpectation.fulfill() })
        sut.setFilterList(forSource: source, toContentBlocker: cbId, completion: { _ in setExpectation.fulfill() })
        sut.setFilterList(forSource: source, toContentBlocker: cbId, completion: { _ in setExpectation.fulfill() })

        DispatchQueue.global().async {
            decorateee.completeOperation(with: .success, at: 0)
        }

        DispatchQueue.global().async {
            decorateee.completeOperation(with: .anyFailure, at: 1)
        }

        DispatchQueue.global().async {
            decorateee.completeOperation(with: .success, at: 2)
        }

        wait(for: [setExpectation], timeout: 1.0)
        XCTAssertFalse(sut.isExecuting, "Expected to not be executing")
    }

    func makeSUT() -> (FilterListSetterWithExecutionStatus, FilterListSetterMock) {
        let decoratee = FilterListSetterMock()
        let sut = FilterListSetterWithExecutionStatus(decoratee: decoratee)
        trackForMemoryLeaks(sut)
        return (sut, decoratee)
    }
}
