/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

import XCTest
@testable import ABPKitSafari

class ShortCircuitedFilterListLoaderTests: XCTestCase {

    func test_load_notCircuited_executesFilterListLoad() {
        let (sut, decoratee) = makeSUT(shortCircuit: { false })

        let source = anyFilterListSource()
        sut.loadFilterList(forSource: source,
                           completion: { _ in })
        XCTAssertEqual(decoratee.requestedParams, [source])
    }

    func test_load_decorateeCompleted_forwardsTheResult() {
        let (sut, decoratee) = makeSUT(shortCircuit: { false })

        expectOperation(curry(sut.loadFilterList)(anyFilterListSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .anyFailure, at: 0)
        })

        expectOperation(curry(sut.loadFilterList)(anyFilterListSource()),
                        toCompleteWith: .success(anyFilterList()),
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .success(anyFilterList()), at: 1)
        })
    }

    func test_load_shortCircuited_doesNotExecuteFilterListLoad() {
        let (sut, decoratee) = makeSUT(shortCircuit: { true })

        expectOperation(curry(sut.loadFilterList)(anyFilterListSource()),
                        toCompleteWith: .anyFailureWithCode(.executionCancelled),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))

        XCTAssertTrue(decoratee.requestedParams.isEmpty, "Expected to not request load when shortcircuited")
    }

    // MARK: - Helpers

    private func makeSUT(shortCircuit: @escaping ShortCircuit) -> (ShortCircuitedFilterListLoader, FilterListLoaderMock) {
        let decoratee = FilterListLoaderMock()
        let sut = ShortCircuitedFilterListLoader(decoratee: decoratee,
                                                 shortCircuit: shortCircuit)
        trackForMemoryLeaks(sut)
        return (sut, decoratee)
    }
}
