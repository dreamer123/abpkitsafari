/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

import XCTest
@testable import ABPKitSafari

class ShortCircuitedExtensionReloaderTests: XCTestCase {

    func test_reload_notCircuited_executesExtensionReload() {
        let (sut, decoratee) = makeSUT(shortCircuit: { false })

        let cbId = anyContentBlockerId()

        sut.reloadContentBlocker(forId: cbId, completion: { _ in })
        XCTAssertEqual(decoratee.requestedParams, [cbId])
    }

    func test_reload_decorateeCompleted_forwardsTheResult() {
        let (sut, decoratee) = makeSUT(shortCircuit: { false })

        expectOperation(curry(sut.reloadContentBlocker)(anyContentBlockerId()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .anyFailure, at: 0)
        })

        expectOperation(curry(sut.reloadContentBlocker)(anyContentBlockerId()),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .success, at: 1)
        })
    }

    func test_reload_shortCircuited_doesNotExecuteExtensionReload() {
        let (sut, decoratee) = makeSUT(shortCircuit: { true })

        expectOperation(curry(sut.reloadContentBlocker)(anyContentBlockerId()),
                        toCompleteWith: .anyFailureWithCode(.executionCancelled),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))

        XCTAssertTrue(decoratee.requestedParams.isEmpty, "Expected to not request reload when shortcircuited")
    }

    // MARK: - Helpers

    private func makeSUT(shortCircuit: @escaping ShortCircuit) -> (ShortCircuitedExtensionReloader, ExtensionReloaderMock) {
        let decoratee = ExtensionReloaderMock()
        let sut = ShortCircuitedExtensionReloader(decoratee: decoratee,
                                                      shortCircuit: shortCircuit)
        trackForMemoryLeaks(sut)
        return (sut, decoratee)
    }

    private func anyActiveSource() -> ContentBlockerActiveSource {
        ContentBlockerActiveSource(source: anyFilterListSource(),
                                   cbId: anyContentBlockerId())
    }
}
