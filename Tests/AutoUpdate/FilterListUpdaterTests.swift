// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitSafari
import XCTest

class FilterListUpdaterTests: XCTestCase {

    func test_update_failedToGetSourcesToUpdate_doesNotPerformUpdates() {

        let (sut, sourcesToUpdateProvider, filterListSetter) = makeSUT()

        sut { _ in }
        sourcesToUpdateProvider.completeLoading(with: .anyFailure)

        XCTAssertTrue(filterListSetter.requests.isEmpty,
                      "Expected no update request if failed to load sources to update")
    }

    func test_update_failedToGetSourcesToUpdate_emitsNoEvents() {
        let (sut, sourcesToUpdateProvider, _) = makeSUT()

        var collectedEvents = [FilterListUpdateEvent]()
        sut { event in collectedEvents.append(event) }
        sourcesToUpdateProvider.completeLoading(with: .anyFailure)

        XCTAssertTrue(collectedEvents.isEmpty,
                      "Expected to emit no update events if failed to get sources for update")
    }

    func test_update_loadedSourcesToUpdate_noSources_noUpdates() {
        let (sut, sourcesToUpdateProvider, filterListSetter) = makeSUT()

        sut { _ in }
        sourcesToUpdateProvider.completeLoading(with: .success([]))

        XCTAssertTrue(filterListSetter.requests.isEmpty,
                      "Expected no load request if no sources to update")
    }

    func test_update_multipleActiveSources_performsCorrectUpdates() {
        let (sut, sourcesToUpdateProvider, filterListSetter) = makeSUT()

        let remoteActiveSource1 = ContentBlockerSourceToUpdate(source: .abp(.withAA(.easyList)),
                                                               cbId: "com.sampleid")
        let remoteActiveSource2 = ContentBlockerSourceToUpdate(source: .abp(.withoutAA(.easyList)),
                                                               cbId: "com.sampleid2")
        let expectSourcesToSet = [FilterListSetRequest(source: .remote(remoteActiveSource1.source),
                                                       cbId: remoteActiveSource1.cbId),
                                  FilterListSetRequest(source: .remote(remoteActiveSource2.source),
                                                       cbId: remoteActiveSource2.cbId)]

        sut { _ in }
        sourcesToUpdateProvider.completeLoading(with: .success([remoteActiveSource1, remoteActiveSource2]))

        XCTAssertEqual(filterListSetter.requestedParams,
                       expectSourcesToSet,
                       "When having multiple requests, expected to make \(expectSourcesToSet), instead made \(filterListSetter.requestedParams) requests")
    }

    func test_update_multipleSources_emitsCorrectEvents() {
        // Arrange
        let (sut, sourcesToUpdateProvider, filterListSetter) = makeSUT()

        let remoteActiveSource0 = ContentBlockerSourceToUpdate(source: .abp(.withAA(.easyList)),
                                                               cbId: "com.sampleid")
        let remoteActiveSource1 = ContentBlockerSourceToUpdate(source: .abp(.withoutAA(.easyList)),
                                                               cbId: "com.sampleid2")

        // Act
        var collectedEvents = [FilterListUpdateEvent]()
        sut { event in collectedEvents.append(event) }

        sourcesToUpdateProvider.completeLoading(with: .success([remoteActiveSource0, remoteActiveSource1]))

        filterListSetter.completeOperation(with: .success, at: 0)
        filterListSetter.completeOperation(with: .anyFailure, at: 1)

        // Assert
        let expectedEvents: [FilterListUpdateEvent] = [.startedToUpdate(source: remoteActiveSource0),
                                                       .startedToUpdate(source: remoteActiveSource1),
                                                       .completedToUpdate(source: remoteActiveSource0,
                                                                          result: .success),
                                                       .completedToUpdate(source: remoteActiveSource1,
                                                                          result: .anyFailure)]
        XCTAssertEqual(collectedEvents,
                       expectedEvents,
                       "Expected to emit events \(expectedEvents), instead emitted \(collectedEvents)")
    }

    func test_update_manualSetIsExecuting_doesNotPerformUpdates() {
        let manualSetIsExecuting = true
        let (sut, sourcesToUpdateProvider, filterListSetter) = makeSUT({ manualSetIsExecuting })

        sut { _ in }

        XCTAssertTrue(sourcesToUpdateProvider.requests.isEmpty, "Expected no sources to update request if manual set is executing")
        XCTAssertTrue(filterListSetter.requests.isEmpty,
                      "Expected no update request if manual set is executing")
    }

    func test_update_manualSetStartedDuringUpdate_notifiesWithCancelledUpdateForCancelledFilterListSet() {

        var manualSetIsExecuting = false

        let (sut, sourcesToUpdateProvider, filterListSetter) = makeSUT({ manualSetIsExecuting })

        var collectedEvents = [FilterListUpdateEvent]()
        sut { event in collectedEvents.append(event) }

        let remoteActiveSource0 = ContentBlockerSourceToUpdate(source: .abp(.withAA(.easyList)),
                                                               cbId: "com.sampleid")
        let remoteActiveSource1 = ContentBlockerSourceToUpdate(source: .abp(.withoutAA(.easyList)),
                                                               cbId: "com.sampleid1")

        sourcesToUpdateProvider.completeLoading(with: .success([remoteActiveSource0, remoteActiveSource1]))

        filterListSetter.completeOperation(with: .success, at: 0)
        manualSetIsExecuting = true
        filterListSetter.completeOperation(with: .anyFailure, at: 1)

        let expectedEvents: [FilterListUpdateEvent] = [.startedToUpdate(source: remoteActiveSource0),
                                                       .startedToUpdate(source: remoteActiveSource1),
                                                       .completedToUpdate(source: remoteActiveSource0,
                                                                          result: .success),
                                                       .cancelledUpdate(source: remoteActiveSource1)]
        XCTAssertEqual(collectedEvents, expectedEvents)

    }

    // MARK: - Helpers

    private func makeSUT(_ manualSetIsExecuting: @escaping () -> Bool = { false }) -> (FilterListUpdater, SourcesToUpdateProviderMock,
        FilterListSetterMock) {
            let sourcesToUpdateProvider = SourcesToUpdateProviderMock()
            let filterListSetter = FilterListSetterMock()
            let sut = buildFilterListUpdater(sourcesToUpdateProvider: sourcesToUpdateProvider,
                                             filterListSetter: filterListSetter,
                                             shortCircuit: manualSetIsExecuting)
            return (sut, sourcesToUpdateProvider, filterListSetter)
    }

    private class SourcesToUpdateProviderMock: SourcesToUpdateProvider {
        var requests = [GetCompletion]()

        func get(completion: @escaping GetCompletion) {
            requests.append(completion)
        }

        func completeLoading(with result: GetResult) {
            requests[0](result)
        }
    }
}
