/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

import XCTest
@testable import ABPKitSafari

class ShortCircuitedActiveFilterListSaverTests: XCTestCase {
    func test_save_notCircuited_executesFilterListSave() {
        let (sut, decoratee) = makeSUT(shortCircuit: { false })

        let activeFilterList = anyActiveFilterList()

        sut.saveActiveFilterList(activeFilterList, completion: { _ in })
        XCTAssertEqual(decoratee.requestedParams, [activeFilterList])
    }

    func test_save_decorateeCompleted_forwardsTheResult() {
        let (sut, decoratee) = makeSUT(shortCircuit: { false })

        expectOperation(curry(sut.saveActiveFilterList)(anyActiveFilterList()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .anyFailure, at: 0)
        })

        expectOperation(curry(sut.saveActiveFilterList)(anyActiveFilterList()),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .success, at: 1)
        })
    }

    func test_save_shortCircuited_doesNotExecuteFilterListSave() {
        let (sut, decoratee) = makeSUT(shortCircuit: { true })

        expectOperation(curry(sut.saveActiveFilterList)(anyActiveFilterList()),
                        toCompleteWith: .anyFailureWithCode(.executionCancelled),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))

        XCTAssertTrue(decoratee.requestedParams.isEmpty, "Expected to not request save when shortcircuited")
    }

    // MARK: - Helpers

    private func makeSUT(shortCircuit: @escaping ShortCircuit) -> (ActiveFilterListSaver, ActiveFilterListSaverMock) {
        let decoratee = ActiveFilterListSaverMock()
        let sut = ShortCircuitedActiveFilterListSaver(decoratee: decoratee,
                                                      shortCircuit: shortCircuit)
        trackForMemoryLeaks(sut)
        return (sut, decoratee)
    }

    private func anyActiveFilterList() -> ActiveFilterList {
        ActiveFilterList(filterList: anyFilterList(),
                         cbIdentifier: anyContentBlockerId())
    }
}
