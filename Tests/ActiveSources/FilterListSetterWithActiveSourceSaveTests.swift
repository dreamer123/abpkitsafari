/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

@testable import ABPKitSafari
import XCTest

class FilterListSetterWithActiveSourceSaveTests: XCTestCase {

    func test_updateRules_requestsUpdateForCorrectSource() {
        let (sut, updater, _) = makeSUT()

        let source = anyFilterListSource()
        let cbId = anyContentBlockerId()
        sut.setFilterList(forSource: source,
                          toContentBlocker: cbId,
                          completion: { _ in })

        XCTAssertEqual(updater.requestedParams, [.init(source: source,
                                                       cbId: cbId)])
    }

    func test_updateRules_deorateeUpdateCompleted_completesWithCorrectResult() {
        let (sut, updater, _) = makeSUT()

        expectOperation(curry(sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            updater.completeOperation(with: .anyFailure, at: 0)
        })

        expectOperation(curry(sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==),
                        when: {
                            updater.completeOperation(with: .success, at: 1)
        })
    }

    func test_updateRules_updateFailed_doesNotAssociatesSourceWithCBId() {
        let (sut, updater, activeSourceSaver) = makeSUT()

        let source = anyFilterListSource()
        let cbId = anyContentBlockerId()
        sut.setFilterList(forSource: source,
                          toContentBlocker: cbId,
                          completion: { _ in })
        updater.completeOperation(with: .anyFailure)
        XCTAssertEqual(activeSourceSaver.requestedParams, [])
    }

    func test_updateRules_onSuccessfullUpdate_associatesSourceWithCBId() {
        let (sut, updater, activeSourceSaver) = makeSUT()

        let source = anyFilterListSource()
        let cbId = anyContentBlockerId()
        sut.setFilterList(forSource: source,
                          toContentBlocker: cbId,
                          completion: { _ in })
        updater.completeOperation(with: .success)
        XCTAssertEqual(activeSourceSaver.requestedParams, [.init(source: source, cbId: cbId)])
    }

    func test_updateRules_saveResultDoesNotAffectUpdateResult() {
        let (sut, updater, activeSourceSaver) = makeSUT()

        expectOperation(curry(sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==),
                        when: {
                            updater.completeOperation(with: .success)
                            activeSourceSaver.completeOperation(with: .anyFailure)
        })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> (FilterListSetter, FilterListSetterMock, ActiveSourceSaverMock) {
        let decoratee = FilterListSetterMock()
        let activeSourceSaver = ActiveSourceSaverMock()
        let sut = FilterListSetterWithActiveSourceSave(decoratee: decoratee,
                                                       activeSourceSaver: activeSourceSaver)

        trackForMemoryLeaks(sut, file: file, line: line)

        return (sut, decoratee, activeSourceSaver)
    }
}
