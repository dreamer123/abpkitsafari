// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitSafari
import XCTest

class ActiveSourcesToUpdateProviderTests: XCTestCase {

    func test_get_requestActiveSourcesLoad() {
        let (sut, _, cbActiveSourcesLoader) = makeSUT()

        sut.get(completion: { _ in })
        XCTAssertTrue(cbActiveSourcesLoader.requestsCount == 1,
                      "Expected to make active sources load request on get")
    }

    func test_get_failedToLoadActiveSources_throwsError() {
        let (sut, _, cbActiveSourcesLoader) = makeSUT()

        expectOperation(sut.get,
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: { cbActiveSourcesLoader.completeLoading(with: .anyFailure) })
    }

    func test_get_nonRemoteSources_returnsEmpty() {
        let (sut, _, cbActiveSourcesLoader) = makeSUT()

        let nonRemoteSource = anyContentBlockerActiveSource(source: anyNonRemoteFilterListSource())

        expectOperation(sut.get,
                        toCompleteWith: .success([]),
                        isEqual: resultIsEqual(==),
                        when: { cbActiveSourcesLoader.completeLoading(with: .success([nonRemoteSource])) })
    }

    func test_get_requestsExpiredSourcesLoad() {
        let (sut, expiredSourcesLoader, cbActiveSourcesLoader) = makeSUT()

        let source = anyRemoteFilterListSource()
        let remoteSource = anyContentBlockerActiveSource(source: .remote(source))

        sut.get(completion: { _ in })
        cbActiveSourcesLoader.completeLoading(with: .success([remoteSource]))

        XCTAssertEqual(expiredSourcesLoader.requestsCount,
                       1,
                       "Once active sources are loaded expected to ask for expired sources")
    }

    func test_get_noExpiredSource_returnsEmpty() {
        let (sut, expiredSourcesLoader, cbActiveSourcesLoader) = makeSUT()

        let source = anyRemoteFilterListSource()
        let remoteSource = anyContentBlockerActiveSource(source: .remote(source))

        expectOperation(sut.get,
                        toCompleteWith: .success([]),
                        isEqual: resultIsEqual(==),
                        when: {
                            cbActiveSourcesLoader.completeLoading(with: .success([remoteSource]))
                            expiredSourcesLoader.completeLoading(with: .success([])) })
    }

    func test_get_needsUpdate_returnsSource() {
        let (sut, refreshChecker, cbActiveSourcesLoader) = makeSUT()

        let source = anyRemoteFilterListSource()
        let cbId = anyContentBlockerId()
        let remoteSource = ContentBlockerActiveSource(source: .remote(source), cbId: cbId)

        expectOperation(sut.get,
                        toCompleteWith: .success([ContentBlockerSourceToUpdate(source: source, cbId: cbId)]),
                        isEqual: resultIsEqual(==),
                        when: {
                            cbActiveSourcesLoader.completeLoading(with: .success([remoteSource]))
                            refreshChecker.completeLoading(with: .success([source])) })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> (ActiveSourcesToUpdateProvider, FilterListExpiredSourcesLoaderMock, ActiveSourceLoaderMock)  {
        let filterListExpiredSourcesLoader = FilterListExpiredSourcesLoaderMock()
        let cbActiveSourcesLoader = ActiveSourceLoaderMock()
        let sut = ActiveSourcesToUpdateProvider(expiredSourcesLoader: filterListExpiredSourcesLoader,
                                                activeSourcesLoader: cbActiveSourcesLoader)
        trackForMemoryLeaks(sut, file: file, line: line)
        return (sut, filterListExpiredSourcesLoader, cbActiveSourcesLoader)
    }

    class ActiveSourceLoaderMock: ActiveSourcesLoader {
        private var requests = [LoadCompletion]()

        var requestsCount: Int {
            requests.count
        }

        func loadActiveSources(completion: @escaping LoadCompletion) {
            requests.append(completion)
        }

        func completeLoading(with result: LoadResult) {
            requests[0](result)
        }
    }

    private class FilterListExpiredSourcesLoaderMock: FilterListExpiredSourcesLoader {
        private var requests = [LoadCompletion]()

        var requestsCount: Int {
            requests.count
        }

        func loadExpiredSources(completion: @escaping LoadCompletion) {
            requests.append(completion)
        }

        func completeLoading(with result: LoadResult) {
            requests[0](result)
        }
    }
}
