/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

@testable import ABPKitSafari
import XCTest

class FileSystemActiveSourceStoreTests: XCTestCase {

    override func setUp() {
        super.setUp()

        try? FileManager.default.removeItem(at: storeTestingURL())
    }

    override func tearDown() {
        super.tearDown()

        try? FileManager.default.removeItem(at: storeTestingURL())
    }

    func test_emptyCache_retrieveThrowsError() {
        let sut = makeSUT(storeURL: storeTestingURL())

        expectOperation(sut.retrieve,
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==))
    }

    func test_emptyCache_noSideEffects() {
        let sut = makeSUT(storeURL: storeTestingURL())

        expectOperation(sut.retrieve,
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==))
        expectOperation(sut.retrieve,
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==))
    }

    func test_insertAfterRetrieve_returnsInsertedSources() {
        let sut = makeSUT(storeURL: storeTestingURL())

        let insertedSources = [anyContentBlockerActiveSource()]
        expectOperation(curry(sut.insert)(insertedSources),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==))
        expectOperation(sut.retrieve,
                        toCompleteWith: .success(insertedSources),
                        isEqual: resultIsEqual(==))
    }

    func test_sideEffects_runSerially() throws {
        let sut = makeSUT(storeURL: storeTestingURL())

        let sources = [anyContentBlockerActiveSource()]

        var orderedExpectations = [XCTestExpectation]()

        let exp1 = expectation(description: "exp1")
        sut.insert(sources) { _ in
            orderedExpectations.append(exp1)
            exp1.fulfill()
        }

        let exp2 = expectation(description: "exp2")
        sut.insert(sources) { _ in
            orderedExpectations.append(exp2)
            exp2.fulfill()
        }

        let exp3 = expectation(description: "exp3")
        sut.insert(sources) { _ in
            orderedExpectations.append(exp3)
            exp3.fulfill()
        }

        waitForExpectations(timeout: 1.0)
        XCTAssertEqual(orderedExpectations,
                       [exp1, exp2, exp3],
                       "Expected side-effects to run serially, operations finished in wrong order")
    }

    func test_insert_failedToWrite_throwsFailedToInsertActiveSourceError() throws {
        let invalidStoreURL = try XCTUnwrap(URL(string: "invallid://store"))
        let sut = makeSUT(storeURL: invalidStoreURL)

        let insertedSources = [anyContentBlockerActiveSource()]
        expectOperation(curry(sut.insert)(insertedSources),
                        toCompleteWith: .anyFailureWithCode(.failedToCacheActiveSources),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))
    }

    func test_retrieve_failedToRead_throwsFailedToLoadActiveSourceError() throws {
        let invalidStoreURL = try XCTUnwrap(URL(string: "Invalid://store"))
        let sut = makeSUT(storeURL: invalidStoreURL)

        expectOperation(sut.retrieve,
                        toCompleteWith: .anyFailureWithCode(.failedToLoadActiveSources),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))
    }

    func test_retrieve_malformedData_throwsFailedToDecodeActiveSourcesError() {
        let storeURL = storeTestingURL()
        let sut = makeSUT(storeURL: storeURL)

        try? "invalid data".data(using: .utf8)?.write(to: storeURL)

        expectOperation(sut.retrieve,
                        toCompleteWith: .anyFailureWithCode(.failedToDecodeActiveSources),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))
    }

    // MARK: - Helpers

    private func makeSUT(storeURL: URL,
                         file: StaticString = #file,
                         line: UInt = #line) -> FileSystemActiveSourcesStore {
        let sut = FileSystemActiveSourcesStore(storeURL: storeURL)

        trackForMemoryLeaks(sut, file: file, line: line)

        return sut
    }

    func storeTestingURL() -> URL {
        FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0].appendingPathComponent("ActiveSourcesTest.store")
    }
}
