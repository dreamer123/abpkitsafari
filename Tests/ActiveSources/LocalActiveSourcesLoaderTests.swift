// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitSafari
import XCTest

class LocalActiveSourcesLoaderTests: XCTestCase {

    func test_load_retrievesTheSourcesFromStore() {
        let (sut, store) = makeSut()

        sut.loadActiveSources { _ in }

        XCTAssertEqual(store.retrieveRequestCount, 1)
    }

    func test_load_retrieveFailed_throwsError() {
        let (sut, store) = makeSut()

        expectOperation(sut.loadActiveSources,
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            store.completeRetrieve(withResult: .anyFailure)
        })
    }

    func test_load_retrieveSucceeded_completesWithSources() {
        let (sut, store) = makeSut()

        let storedContentBlockerSource = anyContentBlockerActiveSource()
        expectOperation(sut.loadActiveSources,
                        toCompleteWith: .success([storedContentBlockerSource]),
                        isEqual: resultIsEqual(==),
                        when: {
                            store.completeRetrieve(withResult: .success([storedContentBlockerSource]))
        })
    }

    func test_insert_retrievesTheCurrentStoredSources() {
        let (sut, store) = makeSut()

        sut.saveActiveSource(anyContentBlockerActiveSource(), completion: { _ in })

        XCTAssertEqual(store.retrieveRequestCount, 1)
    }

    func test_insert_retrievesTheCurrentStoredSourcesFailed_insertsNewSources() {
        let (sut, store) = makeSut()

        let contentBlockerSource = anyContentBlockerActiveSource()
        sut.saveActiveSource(contentBlockerSource, completion: { _ in })
        store.completeRetrieve(withResult: .anyFailure)

        XCTAssertEqual(store.insertRequests, [[contentBlockerSource]])
    }

    func test_insert_retrievesTheCurrentStoredSources_appendsNewSources() {
        let (sut, store) = makeSut()

        let newContentBlockerSource = anyContentBlockerActiveSource(source: anyFilterListSource(),
                                                      cbId: "cbId1")
        let storedContentBlockerSource = anyContentBlockerActiveSource(source: anyFilterListSource(),
                                                         cbId: "cbId2")
        sut.saveActiveSource(newContentBlockerSource, completion: { _ in })
        store.completeRetrieve(withResult: .success([storedContentBlockerSource]))

        XCTAssertEqual(store.insertRequests, [[storedContentBlockerSource, newContentBlockerSource]])
    }

    func test_insert_sourcePresentForCbId_updatesTheSource() {
        let (sut, store) = makeSut()

        let cbId = anyContentBlockerId()
        let oldFilterListSource = anyFilterListSource()
        let oldContentBlockerSource = anyContentBlockerActiveSource(source: oldFilterListSource,
                                                                    cbId: cbId)

        let newSource = FilterListSource.remote(.abp(.withAA(.easyList)))
        let newContentBlockerSource = anyContentBlockerActiveSource(source: newSource,
                                                                    cbId: cbId)
        sut.saveActiveSource(newContentBlockerSource, completion: { _ in })
        store.completeRetrieve(withResult: .success([oldContentBlockerSource]))

        XCTAssertEqual(store.insertRequests, [[newContentBlockerSource]])
    }

    func test_insert_insertToStoreFailed_throwsError() {
        let (sut, store) = makeSut()

        expectOperation(curry(sut.saveActiveSource)(anyContentBlockerActiveSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            store.completeRetrieve(withResult: .anyFailure)
                            store.completeInsert(withResult: .anyFailure)
        })
    }

    func test_insert_insertToStoreSucceeded_completesWithSuccess() {
        let (sut, store) = makeSut()

        expectOperation(curry(sut.saveActiveSource)(anyContentBlockerActiveSource()),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==),
                        when: {
                            store.completeRetrieve(withResult: .anyFailure)
                            store.completeInsert(withResult: .success)
        })
    }

    // MARK: - Helpers

    private func makeSut(file: StaticString = #file,
                         line: UInt = #line) -> (LocalActiveSourcesLoader, ActiveSourcesStoreMock) {
        let store = ActiveSourcesStoreMock()
        let loader = LocalActiveSourcesLoader(store: store)

        trackForMemoryLeaks(loader, file: file, line: line)

        return (loader, store)
    }

    private class ActiveSourcesStoreMock: ActiveSourcesStore {
        private var retrieveCompletions = [RetrieveCompletion]()
        private var insertCompletions = [InsertCompletion]()

        private(set) var insertRequests = [[ContentBlockerActiveSource]]()

        var retrieveRequestCount: Int {
            retrieveCompletions.count
        }

        func retrieve(completion: @escaping RetrieveCompletion) {
            retrieveCompletions.append(completion)
        }

        func insert(_ sources: [ContentBlockerActiveSource], completion: @escaping InsertCompletion) {
            insertRequests.append(sources)
            insertCompletions.append(completion)
        }

        func completeRetrieve(withResult result: RetrieveResult, at idx: Int = 0) {
            retrieveCompletions[idx](result)
        }

        func completeInsert(withResult result: InsertResult, at idx: Int = 0) {
            insertCompletions[idx](result)
        }
    }
}
