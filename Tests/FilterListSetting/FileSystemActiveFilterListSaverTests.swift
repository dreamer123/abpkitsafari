// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitSafari
import XCTest

class FileSystemActiveFilterListSaverTests: XCTestCase {
    func test_save_writesTheFilterListWithCorrectName() {
        let containerURL = filterListContainerTestURL()
        let sut = makeSUT(filterListStoreURL: containerURL)

        let filterList = anyFilterList()
        let cbId = anyContentBlockerId()
        let activeFilterList = ActiveFilterList(filterList: filterList,
                                                              cbIdentifier: cbId)
        let saveExp = expectation(description: #function)
        sut.saveActiveFilterList(activeFilterList, completion: { _ in
            saveExp.fulfill()
        })
        wait(for: [saveExp], timeout: 1.0)

        let expecteSaveURL = containerURL.appendingPathComponent("\(cbId).json")
        XCTAssertTrue(FileManager.default.fileExists(atPath: expecteSaveURL.path))

        try? FileManager.default.removeItem(at: expecteSaveURL)
    }

    func test_save_writesCorrectFilterList() throws {
        let containerURL = filterListContainerTestURL()
        let sut = makeSUT(filterListStoreURL: containerURL)

        let filterList = anyFilterList()
        let cbId = anyContentBlockerId()
        let activeFilterList = ActiveFilterList(filterList: filterList,
                                                              cbIdentifier: cbId)
        let saveExp = expectation(description: #function)
        sut.saveActiveFilterList(activeFilterList, completion: { _ in
            saveExp.fulfill()
        })
        wait(for: [saveExp], timeout: 1.0)

        let expecteSaveURL = containerURL.appendingPathComponent("\(cbId).json")

        let savedRawFilterList = try XCTUnwrap(String(contentsOf: expecteSaveURL, encoding: .utf8))
        XCTAssertEqual(savedRawFilterList, filterList.content)
        try? FileManager.default.removeItem(at: expecteSaveURL)
    }

    func test_sideEffects_runSerially() throws {
        let sut = makeSUT(filterListStoreURL: filterListContainerTestURL())

        let activeFilterList = ActiveFilterList(filterList: anyFilterList(),
                                                              cbIdentifier: anyContentBlockerId())

        var orderedExpectations = [XCTestExpectation]()

        let exp1 = expectation(description: "exp1")
        sut.saveActiveFilterList(activeFilterList) { _ in
            orderedExpectations.append(exp1)
            exp1.fulfill()
        }

        let exp2 = expectation(description: "exp2")
        sut.saveActiveFilterList(activeFilterList) { _ in
            orderedExpectations.append(exp2)
            exp2.fulfill()
        }

        let exp3 = expectation(description: "exp3")
        sut.saveActiveFilterList(activeFilterList) { _ in
            orderedExpectations.append(exp3)
            exp3.fulfill()
        }

        waitForExpectations(timeout: 1.0)
        XCTAssertEqual(orderedExpectations,
                       [exp1, exp2, exp3],
                       "Expected side-effects to run serially, operations finished in wrong order")
    }

    // MARK: - Helpers

    private func makeSUT(filterListStoreURL: URL,
                         file: StaticString = #file,
                         line: UInt = #line) -> FileSystemActiveFilterListSaver {
        let sut = FileSystemActiveFilterListSaver(containerURL: filterListStoreURL)

        trackForMemoryLeaks(sut, file: file, line: line)

        return sut
    }

    private func filterListContainerTestURL() -> URL {
        FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
    }
}
