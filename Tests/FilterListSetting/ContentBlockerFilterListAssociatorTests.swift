// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import XCTest
@testable import ABPKitSafari

class ContentBlockerFilterListAssociatorTests: XCTestCase {

    func test_load_performsCorrectRequestsOnLoader() {
        let (sut, filterListLoader, _) = makeSUT()

        let source = anyFilterListSource()
        sut.setFilterList(forSource: source,
                          toContentBlocker: anyContentBlockerId(),
                          completion: { _ in })
        XCTAssertTrue(filterListLoader.requestedParams == [source],
                      "Expected to request rules load for \(source), instead requested for \(filterListLoader.requestedParams)")
    }

    func test_load_onFilterListLoadErrorCompletesWithError() {
       let (sut, filterListLoader, _) = makeSUT()

        expectOperation(curry(sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            filterListLoader.completeOperation(with: .anyFailure)
        })
    }

    func test_load_requestSaveForLoadedFilterList() {
        let (sut, filterListLoader, activeFilterListSaver) = makeSUT()

        let source = anyFilterListSource()
        let cbId = anyContentBlockerId()
        let filterList = anyFilterList()
        sut.setFilterList(forSource: source,
                          toContentBlocker: cbId,
                          completion: { _ in })
        filterListLoader.completeOperation(with: .success(filterList))

        let expectedActiveFilterList = ActiveFilterList(filterList: filterList,
                                                                      cbIdentifier: cbId)
        XCTAssertEqual(activeFilterListSaver.requestedParams, [expectedActiveFilterList])
    }

    func test_load_activeFilterListSaveFailed_throwsError() {
        let (sut, filterListLoader, activeFilterListSaver) = makeSUT()

        expectOperation(curry(sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            filterListLoader.completeOperation(with: .success(anyFilterList()))
                            activeFilterListSaver.completeOperation(with: .anyFailure)
        })
    }

    func test_load_activeFilterListSaveSucceeded_completeWithSuccess() {
        let (sut, filterListLoader, activeFilterListSaver) = makeSUT()

        expectOperation(curry(sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==),
                        when: {
                            filterListLoader.completeOperation(with: .success(anyFilterList()))
                            activeFilterListSaver.completeOperation(with: .success)
        })
    }

    // MARK: - Private methods

    private func makeSUT(file: StaticString = #file, line: UInt = #line) -> (FilterListToContentBlockerAssociator, FilterListLoaderMock, ActiveFilterListSaverMock) {
        let filterListLoader = FilterListLoaderMock()
        let filterListSaver = ActiveFilterListSaverMock()
        let sut = FilterListToContentBlockerAssociator(filterListLoader: filterListLoader, activeFilterListSaver: filterListSaver)

        trackForMemoryLeaks(sut, file: file, line: line)

        return (sut, filterListLoader, filterListSaver)
    }
}
