// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitSafari
import XCTest

class ContentBlockerFilterListSetterIntegrationTests: XCTestCase {

    func test_setFilterList_loadsFilterListForCorrectSource() {
        let inputs = makeSUT()

        let source = anyFilterListSource()
        inputs.sut.setFilterList(forSource: source, toContentBlocker: anyContentBlockerId(), completion: { _ in })

        XCTAssertEqual(inputs.filterListLoader.requestedParams,
                       [source],
                       "Expected to request filter list load for source \(source), instead requested for \(inputs.filterListLoader.requestedParams)")
    }

    func test_setFilterList_loadFilterListFailed_completesWithError() {
        let inputs = makeSUT()

        expectOperation(curry(inputs.sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            inputs.filterListLoader.completeOperation(with: .anyFailure)
        })
    }

    func test_setFilterList_loadFilterListFailed_doesNotSaveActiveSource() {
        let inputs = makeSUT()

        inputs.sut.setFilterList(forSource: anyFilterListSource(),
                                 toContentBlocker: anyContentBlockerId(),
                                 completion: { _ in })

        inputs.filterListLoader.completeOperation(with: .anyFailure)

        XCTAssertTrue(inputs.activeFilterListSaver.requestedParams.isEmpty,
                      "Expected source to not be set as active if filter list load failed")
    }

    func test_setFilterList_loadFilterListFailed_doesNotReloadExtension() {
        let inputs = makeSUT()

        inputs.sut.setFilterList(forSource: anyFilterListSource(),
                                 toContentBlocker: anyContentBlockerId(),
                                 completion: { _ in })
        inputs.filterListLoader.completeOperation(with: .anyFailure)

        XCTAssertTrue(inputs.extensionReloader.requestedParams.isEmpty,
                      "Expected extension to not be reloaded if filter list load failed")
    }

    func test_setFilterList_loadSucceeded_savesTheRules() {
        let inputs = makeSUT()

        let loadedFilterList = anyFilterList()
        let cbId = anyContentBlockerId()
        inputs.sut.setFilterList(forSource: anyFilterListSource(),
                                 toContentBlocker: cbId,
                                 completion: { _ in })
        inputs.filterListLoader.completeOperation(with: .success(loadedFilterList))

        XCTAssertEqual(inputs.activeFilterListSaver.requestedParams,
                       [.init(filterList: loadedFilterList, cbIdentifier: cbId)])
    }

    func test_setFilterList_saveFailed_throwsError() {
        let inputs = makeSUT()

        expectOperation(curry(inputs.sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            inputs.filterListLoader.completeOperation(with: .success(anyFilterList()))
                            inputs.activeFilterListSaver.completeOperation(with: .anyFailure)
        })
    }

    func test_setFilterList_filterListSaveFailed_doesNotSaveActiveSource() {
        let inputs = makeSUT()

        inputs.sut.setFilterList(forSource: anyFilterListSource(),
                                 toContentBlocker: anyContentBlockerId(),
                                 completion: { _ in })
        inputs.filterListLoader.completeOperation(with: .success(anyFilterList()))
        inputs.activeFilterListSaver.completeOperation(with: .anyFailure)
        XCTAssertTrue(inputs.activeSourcesSaver.requestedParams.isEmpty,
                      "Expected source to not be set as active if filter list save failed")
    }

    func test_setFilterList_filterListSaveFailed_doesNotReloadExtension() {
        let inputs = makeSUT()

        inputs.sut.setFilterList(forSource: anyFilterListSource(),
                                 toContentBlocker: anyContentBlockerId(),
                                 completion: { _ in })
        inputs.filterListLoader.completeOperation(with: .success(anyFilterList()))
        inputs.activeFilterListSaver.completeOperation(with: .anyFailure)
        XCTAssertTrue(inputs.extensionReloader.requestedParams.isEmpty,
                      "Expected extension to not be reloaded if filter list save failed")
    }

    func test_setFilterList_filterListSaveSuccess_savesActiveSource() {
        let inputs = makeSUT()

        let source = anyFilterListSource()
        let cbId = anyContentBlockerId()
        inputs.sut.setFilterList(forSource: source,
                                 toContentBlocker: cbId,
                                 completion: { _ in })
        inputs.filterListLoader.completeOperation(with: .success(anyFilterList()))
        inputs.activeFilterListSaver.completeOperation(with: .success)

        XCTAssertEqual(inputs.activeSourcesSaver.requestedParams, [.init(source: source, cbId: cbId)])
    }

    func test_setFilterList_filterListSaveSuccess_reloadsExtension() {
        let inputs = makeSUT()
        let cbIdentifier = anyContentBlockerId()

        inputs.sut.setFilterList(forSource: anyFilterListSource(),
                                 toContentBlocker: cbIdentifier,
                                 completion: { _ in })
        inputs.filterListLoader.completeOperation(with: .success(anyFilterList()))
        inputs.activeFilterListSaver.completeOperation(with: .success)
        XCTAssertEqual(inputs.extensionReloader.requestedParams, [cbIdentifier])
    }

    func test_setFilterList_extensionReloadFailed_completesWithFailure() {
        let inputs = makeSUT()

        expectOperation(curry(inputs.sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            inputs.filterListLoader.completeOperation(with: .success(anyFilterList()))
                            inputs.activeFilterListSaver.completeOperation(with: .success)
                            inputs.extensionReloader.completeOperation(with: .anyFailure)
        })

//        expectSet(fromSource: anyFilterListSource(),
//                  for: anyContentBlockerId(),
//                  toCompleteWith: .anyFailure,
//                  sut: inputs.sut, when: {
//                    inputs.filterListLoader.completeOperation(with: .success(anyFilterList()))
//                    inputs.activeFilterListSaver.completeOperation(with: .success)
//                    inputs.extensionReloader.completeOperation(with: .anyFailure)
//        })
    }

    func test_setFilterList_extensionReloadSucceeded_completesWithSuccess() {
        let inputs = makeSUT()

        expectOperation(curry(inputs.sut.setFilterList)(anyFilterListSource(), anyContentBlockerId()),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==),
                        when: {
                            inputs.filterListLoader.completeOperation(with: .success(anyFilterList()))
                            inputs.activeFilterListSaver.completeOperation(with: .success)
                            inputs.extensionReloader.completeOperation(with: .success)
        })
    }

    // MARK: - Helpers

    private struct TestInputs {
        let sut: FilterListSetter
        let filterListLoader: FilterListLoaderMock
        let activeFilterListSaver: ActiveFilterListSaverMock
        let activeSourcesSaver: ActiveSourceSaverMock
        let extensionReloader: ExtensionReloaderMock
    }

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> TestInputs {
        let filterListLoader = FilterListLoaderMock()
        let activeFilterListSaver = ActiveFilterListSaverMock()
        let activeSourcesSaver = ActiveSourceSaverMock()
        let extensionReloader = ExtensionReloaderMock()

        let sut = FilterListSettingComposer.makeFilterListSetter(filterListLoader: filterListLoader,
                                                                             activeFilterListSaver: activeFilterListSaver,
                                                                             activeSourcesSaver: activeSourcesSaver,
                                                                             extensionReloader: extensionReloader)

        trackForMemoryLeaks(sut as AnyObject, file: file, line: line)

        return TestInputs(sut: sut,
                          filterListLoader: filterListLoader,
                          activeFilterListSaver: activeFilterListSaver,
                          activeSourcesSaver: activeSourcesSaver,
                          extensionReloader: extensionReloader)
    }
}
