// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Useful to unwrap a completion with result
///
/// Provides the ability to work in a more functional manner
///
/// - Parameters:
///   - onSuccess: Function to execute when result is success
///   - onError: Function to execute when result is failure
/// - Returns: A handler for a given result
func handleResult<T>(_ onSuccess: @escaping (T) -> Void,
                     _ onError: @escaping (Error) -> Void) -> (Result<T, Error>) -> Void {
    return {
        switch $0 {
        case let .success(data):
            onSuccess(data)
        case let .failure(error):
            onError(error)
        }
    }
}

/// Executes all of the given functions in parallel and collects the results in one array.
/// Completes once all functionc completed.
/// Assures thread safety.
///
/// - Parameter funcs: A list of functions to be executed in parallel
/// - Returns: A function which when it's executed computes the aggregated results.
func executeInParallel<T>(_ funcs: [(@escaping (T) -> Void) -> Void]) -> (@escaping ([T]) -> Void) -> Void {
    return { completion in
        let updateQueue = DispatchQueue(label: "completionsCollect")
        var results = [T]()

        func executeCompletion(_ data: T) {
            updateQueue.sync {
                results += [data]
                if results.count == funcs.count {
                    completion(results)
                }
            }
        }

        func executeFun(_ f: (@escaping (T) -> Void) -> Void) {
            f(executeCompletion)
        }

        funcs.forEach(executeFun)
    }
}

// MARK: - Decomposition -

func curry<A, B, C>(_ f: @escaping (A, B) -> C) -> (A) -> (B) -> C {
    return { a in
        return { b in
            f(a, b)
        }
    }
}

func curry<A, B, C, D>(_ f: @escaping (A, B, C) -> D) -> (A, B) -> (C) -> D {
    return { a, b in
        return { c in
            f(a, b, c)
        }
    }
}

// MARK: - Composition operators

precedencegroup FunctionsCompositionPrecedence {
    associativity: left
}

infix operator >>>: FunctionsCompositionPrecedence

/// Composes two functions in one
func >>> <A, B, C>(lhs: @escaping (A) -> B, rhs: @escaping (B) -> C) -> (A) -> C {
    return { rhs(lhs($0)) }
}
