// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Describes all of the errors that might occur while using ABPKitSafari
public struct ABPKitSafariError: CustomNSError, Equatable {

    // MARK: - Error info

    /// The error domain, ABPKitSafari specific
    public static var errorDomain: String {
        "ABPKitSafariErrorDomain"
    }

    /// The error code within the given domain. See ABPKitSafariErrorCode for all possible codes
    public let errorCode: Int

    /// The user-info dictionary containing any useful info
    public let errorUserInfo: [String: Any]

    // MARK: - Error Codes

    /// A set of all possible error codes
    public enum ABPKitSafariErrorCode: Int {

        /// Failed to writeh active sources to persistence storage
        case failedToCacheActiveSources

        /// Failed to load active sources from persistence storage
        case failedToLoadActiveSources

        /// Failed to encode active sources to Data
        case failedToEncodeActiveSources

        /// Failed to decode active sources from Data
        case failedToDecodeActiveSources

        /// Content blocker extension reload failed
        case failedToReloadContentBlockerExtension

        /// Active filter list couldn't be found
        case missingActiveFilterList

        /// Failed to write active filter list to persistence storage
        case failedToCacheActiveFilterList

        /// ABPKitSafari wasn't able to build NSItemProvider from active filter list
        case failedToBuildFilterListItemProvider

        /// Operation execution was cancelled
        case executionCancelled
    }

    // MARK: - Private

    private let abpKitCoreErrorCode: ABPKitSafariErrorCode
    private let description: String
    private let underlyingError: Error?

    // MARK: - Init

    init(errorCode: ABPKitSafariErrorCode,
         description: String,
         underlyingError: Error? = nil) {

        self.abpKitCoreErrorCode = errorCode
        self.description = description
        self.underlyingError = underlyingError

        self.errorCode = errorCode.rawValue
        self.errorUserInfo = {
            var errorDict: [String: Any] = [NSLocalizedDescriptionKey: description]
            if let underlyingError = underlyingError {
                errorDict[NSUnderlyingErrorKey] = underlyingError as NSError
            }
            return errorDict
        }()
    }

    // MARK: - Equatable

    public static func == (lhs: ABPKitSafariError, rhs: ABPKitSafariError) -> Bool {
        return (lhs as NSError) == (rhs as NSError)
    }

    // MARK: - Debug info

    var debugDescription: String {
        return "\(description) (\(String(describing: type(of: self))).\(String(describing: abpKitCoreErrorCode)))" +
            (underlyingError.map { "Underlying error: \(($0) as NSError).debugDescription)" } ?? "")
    }
}

// MARK: - Errors Factory

extension ABPKitSafariError {

    static var missingActiveFilterList: ABPKitSafariError {
        .init(errorCode: .missingActiveFilterList,
              description: "{cbIdentifier}.json file is missing, it either was removed from last save or it wasn't saved properly")
    }

    static var failedToBuildFilterListItemProvider: ABPKitSafariError {
        .init(errorCode: .failedToBuildFilterListItemProvider,
              description: "Failed to build NSItemProvider from {cbIdentifier}.json, to be used to inject filter list into Safari")
    }

    static var executionCancelled: ABPKitSafariError {
        .init(errorCode: .executionCancelled,
              description: "Operation execution was cancelled")
    }

    static func failedToCacheActiveSources(_ underlyingError: Error) -> ABPKitSafariError {
        .init(errorCode: .failedToCacheActiveSources,
              description: "There was an error while trying to write active sources to persistence storage. Check underlying error for more information.",
              underlyingError: underlyingError)
    }

    static func failedToLoadActiveSources(_ underlyingError: Error) -> ABPKitSafariError {
        .init(errorCode: .failedToLoadActiveSources,
              description: "There was an error while trying to read active sources to persistence storage. Check underlying error for more information.",
              underlyingError: underlyingError)
    }

    static func failedToEncodeActiveSources(_ underlyingError: Error) -> ABPKitSafariError {
        .init(errorCode: .failedToEncodeActiveSources,
              description: "There was an error while trying to encode active sources to data, see underlying error for more information.",
              underlyingError: underlyingError)
    }

    static func failedToDecodeActiveSources(_ underlyingError: Error) -> ABPKitSafariError {
        .init(errorCode: .failedToDecodeActiveSources,
              description: "There was an error while trying to decode active sources from data, see underlying error for more information.",
              underlyingError: underlyingError)
    }

    static func failedToReloadContentBlockerExtension(_ underlyingError: Error) -> ABPKitSafariError {
        .init(errorCode: .failedToReloadContentBlockerExtension,
              description: "There was an error while trying to reload the content blocker extension, see underlying error for more information.",
              underlyingError: underlyingError)
    }

    static func failedToCacheActiveFilterList(_ underlyingError: Error) -> ABPKitSafariError {
        .init(errorCode: .failedToCacheActiveFilterList,
              description: "There was an error while trying to save the filter list to be used by content blocker extension, see underlying error for more information",
              underlyingError: underlyingError)
    }
}

extension Error {
    var debugDescription: String {
        (self as? ABPKitSafariError)?.debugDescription ?? localizedDescription
    }
}
