// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// The unique identifier of content blocker extension
public typealias ContentBlockerIdentifier = String

/// Handles setting a filter list(loaded from a given source) to a particular content blocker extension
public protocol FilterListSetter {
    /// The result of setting the filter to a content blocker extension, either success or error
    typealias SetResult = Result<Void, Error>
    /// A handler to be used to handle the SetResult
    typealias SetCompletion = (SetResult) -> Void

    /// Sets the filter list loaded for the given source to the given content blocker
    ///
    /// - Parameters:
    ///   - source: Source of filter list to be loaded into the given content blocker
    ///   - cbIdentifier: Identifier of content blocker to set the filter list to
    ///   - completion: Completion of set operation. May be called on any thread!
    func setFilterList(forSource source: FilterListSource,
                       toContentBlocker cbIdentifier: ContentBlockerIdentifier,
                       completion: @escaping SetCompletion)
}
