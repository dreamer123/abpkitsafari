// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Represents the rules associatiated with a given content blocker
struct ActiveFilterList: Equatable {

    /// Filter list associated with the content blocker
    let filterList: FilterList

    /// Content blocker id to which the rules are associated
    let cbIdentifier: ContentBlockerIdentifier
}

/// Provides the functionality of saving a given set of rules as currently active for a given content blocker id
protocol ActiveFilterListSaver {
    typealias SaveResult = Result<Void, Error>
    typealias SaveCompletion = (SaveResult) -> Void

    /// Saves the filter list as currently active for a given content blocker identifier
    ///
    /// - Parameters:
    ///   - activeFilterList: Active filter list to be saved
    ///   - completion: Completion of save operation
    func saveActiveFilterList(_ activeFilterList: ActiveFilterList,
                              completion: @escaping SaveCompletion)
}
