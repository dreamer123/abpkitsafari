// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Describes the contract of a store that contains all of the stored active sources
protocol ActiveSourcesStore {
    typealias RetrieveResult = Result<[ContentBlockerActiveSource], Error>
    typealias RetrieveCompletion = (RetrieveResult) -> Void

    typealias InsertResult = Result<Void, Error>
    typealias InsertCompletion = (InsertResult) -> Void

    /// Retrieve all of the stored active sources
    /// - Parameter completion: A completion of retrieve operation
    func retrieve(completion: @escaping RetrieveCompletion)

    /// Inserts the given active sources to persistent storage
    /// - Parameters:
    ///   - sources: Sources to be stored
    ///   - completion: A completion of store operation
    func insert(_ sources: [ContentBlockerActiveSource], completion: @escaping InsertCompletion)
}
