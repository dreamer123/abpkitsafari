// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Describes the content blocker active source
public struct ContentBlockerActiveSource: Equatable {
    /// Current active source
    public let source: FilterListSource

    /// Content blocker that has the above source as active
    public let cbId: ContentBlockerIdentifier
}

/// Provides the ability to load the currently active sources for all content blockers.
/// ABPKitSafari stores last succesfully set filter list source on a given content blocker,
/// you can use this to restore the state in your app at launch.
///
/// Note: If you don't set the initial source on your content blockers, this will load an empty list
///     of active sources. Thus, be sure to set the initial filter list on your content blockers
///     before using the loader; or use the empty result a hint that you may need to set those.
public protocol ActiveSourcesLoader {
    typealias LoadResult = Result<[ContentBlockerActiveSource], Error>
    typealias LoadCompletion = (LoadResult) -> Void

    /// Loads the currently active sources for all content blockers
    /// - Parameter completion: A completion of load operation
    func loadActiveSources(completion: @escaping LoadCompletion)
}
