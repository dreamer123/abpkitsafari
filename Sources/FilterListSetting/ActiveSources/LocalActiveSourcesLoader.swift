// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Handled the caching logic for active sources
final class LocalActiveSourcesLoader {
    private let store: ActiveSourcesStore

    /// Creates a instance
    /// - Parameter store: The actuall store for the active sources
    init(store: ActiveSourcesStore) {
        self.store = store
    }
}

extension LocalActiveSourcesLoader: ActiveSourcesLoader {
    func loadActiveSources(completion: @escaping LoadCompletion) {
        store.retrieve(completion: completion)
    }
}

extension LocalActiveSourcesLoader: ActiveSourceSaver {
    func saveActiveSource(_ activeSource: ContentBlockerActiveSource, completion: @escaping SaveCompletion) {
        store.retrieve { [weak self] result in
            let storedSources = (try? result.get()) ?? []
            let storedSourceWithoutOldSource = storedSources.filter { $0.cbId != activeSource.cbId }
            let newSources = storedSourceWithoutOldSource + [activeSource]

            self?.store.insert(newSources, completion: completion)
        }
    }
}
