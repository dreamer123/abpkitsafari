// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Proivdes the sources to be updated by checking if the current filter lists are expired or not
final class ActiveSourcesToUpdateProvider: SourcesToUpdateProvider {
    private let expiredSourcesLoader: FilterListExpiredSourcesLoader
    private let activeSourcesLoader: ActiveSourcesLoader

    /// Creates a new instance
    /// - Parameters:
    ///   - expiredSourcesLoader: To be used to retrieve all expired sources
    ///   - activeSourcesLoader: To be used to load active sources for all active content blockers
    init(expiredSourcesLoader: FilterListExpiredSourcesLoader, activeSourcesLoader: ActiveSourcesLoader) {
        self.expiredSourcesLoader = expiredSourcesLoader
        self.activeSourcesLoader = activeSourcesLoader
    }

    func get(completion: @escaping GetCompletion) {
        func completeWithFailure(_ error: Error) {
            completion(.failure(error))
        }

        func completeWithSourcesToUpdate(_ sources: [ContentBlockerSourceToUpdate]) {
            completion(.success(sources))
        }

        func filterOutNonRemoteSources(_ sources: [ContentBlockerActiveSource]) -> [ContentBlockerSourceToUpdate] {
            sources.compactMap {
                if case let .remote(source) = $0.source {
                    return ContentBlockerSourceToUpdate(source: source, cbId: $0.cbId)
                } else {
                    return nil
                }
            }
        }

        func extractSourceThatNeedUpdate() -> ([ContentBlockerSourceToUpdate]) -> Void {
            return { [weak self] sourcesForUpdate in
                guard let self = self else { return }

                guard !sourcesForUpdate.isEmpty else {
                    return completeWithSourcesToUpdate([])
                }

                func matchActiveSourcesToExpiredSources(_ expiredSources: [FilterListType]) {
                    completeWithSourcesToUpdate(sourcesForUpdate.filter { expiredSources.contains($0.source) })
                }

                self.expiredSourcesLoader.loadExpiredSources(completion: handleResult(matchActiveSourcesToExpiredSources,
                                                                                      completeWithFailure))
            }
        }

        let onSourcesLoaded = filterOutNonRemoteSources >>> extractSourceThatNeedUpdate()
        activeSourcesLoader.loadActiveSources(completion: handleResult(onSourcesLoaded,
                                                                       completeWithFailure))
    }
}
