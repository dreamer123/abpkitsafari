/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

/// Represents the specific content blocker reload result based on the
/// active source
public struct ContentBlockerReloadResult {
    /// Infomation about the active source
    public let activeSource: ContentBlockerActiveSource

    /// The actual reload result
    public let result: Result<Void, Error>
}

/// Descirbes the events that occur during content blocker reload trigger by ContentBlockersReloader
public enum ContentBlockerReloadEvent {
    /// Did start the reloading for the given active sources
    case startedReloading(activeSources: [ContentBlockerActiveSource])
    /// Did finish the reload, either by failure or with an array of ContentBlockerReloadResult
    case finishedReloading(result: Result<[ContentBlockerReloadResult], Error>)
}

/// Component responsible for providing the functionality of reloading the content blockers based
/// on currently active sources
public final class ContentBlockersReloader {

    /// There were no active sources set to reload content blocker with
    public struct NoActiveSourcesError: Error, Equatable {}

    private let activeSourcesLoader: ActiveSourcesLoader
    private let filterListSetter: FilterListSetter

    init(activeSourcesLoader: ActiveSourcesLoader, filterListSetter: FilterListSetter) {
        self.activeSourcesLoader = activeSourcesLoader
        self.filterListSetter = filterListSetter
    }

    /// Trigger the content blockers reload.
    /// A content blocker is reloaded with the last active source set through FilterListSetter.
    /// If content blocker has no active source it will not be reload, therefore be sure to set a filter list on all of your
    /// content blocker before using this functionality.
    ///
    /// Note: The function will complete once all content blockers reload completed.
    /// - Parameter completion: The handler for the reload events.
    public func reloadContentBlockers(eventHandler: @escaping (ContentBlockerReloadEvent) -> Void) {
        activeSourcesLoader.loadActiveSources { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .failure(error):
                eventHandler(.finishedReloading(result: .failure(error)))
            case let .success(activeSources):
                guard activeSources.isEmpty == false else {
                    return eventHandler(.finishedReloading(result: .failure(NoActiveSourcesError())))
                }
                eventHandler(.startedReloading(activeSources: activeSources))
                executeInParallel(activeSources.map(self.filterListSet))({ eventHandler(.finishedReloading(result: .success($0)))
                })
            }
        }
    }

    private func filterListSet(_ activeSource: ContentBlockerActiveSource) -> (@escaping (ContentBlockerReloadResult) -> Void) -> Void {
        return { [weak self] completion in
            self?.filterListSetter.setFilterList(forSource: activeSource.source,
                                                 toContentBlocker: activeSource.cbId,
                                                 completion: { result in
                                                    completion(ContentBlockerReloadResult(activeSource: activeSource,
                                                                                          result: result))
            })
        }
    }
}
