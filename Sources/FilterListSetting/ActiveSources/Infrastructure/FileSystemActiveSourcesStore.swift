// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Stores the active sources to file system
final class FileSystemActiveSourcesStore: ActiveSourcesStore {
    private let storeURL: URL
    private let queue = DispatchQueue(label: "\(FileSystemActiveSourcesStore.self).queue",
                                      qos: .userInitiated,
                                      attributes: .concurrent)

    private struct CodableActiveSource: Codable {
        let codableSource: CodableFilterListSource
        let cbId: ContentBlockerIdentifier

        init(activeSource: ContentBlockerActiveSource) {
            codableSource = CodableFilterListSource(source: activeSource.source)
            cbId = activeSource.cbId
        }

        var activeSource: ContentBlockerActiveSource {
            .init(source: codableSource.source, cbId: cbId)
        }
    }

    /// Creates a new instance
    /// - Parameter storeURL: The url of a file where to store the active sources
    init(storeURL: URL) {
        self.storeURL = storeURL
    }

    func retrieve(completion: @escaping RetrieveCompletion) {
        let url = storeURL
        let decoder = JSONDecoder()

        func readContentOfFile() -> Result<Data, Error> {
            Result<Data, Error> {
                try Data(contentsOf: url)
            }.mapError(ABPKitSafariError.failedToLoadActiveSources)
        }

        func decodeLoadedData(data: Data) -> RetrieveResult {
            RetrieveResult {
                let cache = try decoder.decode([CodableActiveSource].self, from: data)
                return cache.map { $0.activeSource }
            }.mapError(ABPKitSafariError.failedToDecodeActiveSources)
        }

        queue.async {
            completion(readContentOfFile().flatMap(decodeLoadedData))
        }
    }

    func insert(_ sources: [ContentBlockerActiveSource], completion: @escaping InsertCompletion) {
        let url = storeURL

        func encodeSources() -> Result<Data, Error> {
            Result<Data, Error> {
                let encoder = JSONEncoder()
                let cache = sources.map(CodableActiveSource.init)
                return try encoder.encode(cache)
            }.mapError(ABPKitSafariError.failedToEncodeActiveSources)
        }

        func writeData(data: Data) -> InsertResult {
            InsertResult {
                try data.write(to: url)
            }.mapError(ABPKitSafariError.failedToCacheActiveSources)
        }

        queue.async(flags: .barrier) {
            completion(encodeSources().flatMap(writeData))
        }
    }
}
