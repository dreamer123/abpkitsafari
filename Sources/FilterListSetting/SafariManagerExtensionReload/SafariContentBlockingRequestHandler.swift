// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// A content blocking request handler to be used to fulfill the SafariContentManager extension reload request
public final class SafariContentBlockingRequestHandler {

    private let filterListContainerURL: URL

    /// Creates an instance
    /// - Parameter extensionRulesURLProvider: To be used get the url from which to load the current active rules for a given content blocker
    init(filterListContainerURL: URL) {
        self.filterListContainerURL = filterListContainerURL
    }

    /// Loads the current active list for the given content blocker identifier and then creates the NSItemProvider
    /// from the loaded list
    ///
    /// - Parameters:
    ///   - cbIdentifier: The content blocker for which to create rules item
    ///   - completion: Completion with result of creating the rules item
    public func getFilterListItem(forContentBlocker cbIdentifier: ContentBlockerIdentifier,
                                  completion: @escaping (Result<NSItemProvider, Error>) -> Void) {
        loadFilterListItem(forContentBlocker: cbIdentifier, completion: completion)
    }

    /// Loads the current active list for the given content blocker identifier and then completes the request with
    /// loaded lists
    ///
    /// - Parameters:
    ///   - context: Context in which to complete the request
    ///   - cbIdentifier: The content blocker for which to update the rules
    ///   - completion: A completion with request completed status
    public func completeExtensionRequest(inContext context: NSExtensionContext,
                                         forContentBlocker cbIdentifier: ContentBlockerIdentifier,
                                         completion: @escaping (Result<Bool, Error>) -> Void) {
        func cancelRequestAndComplete(error: Error) {
            context.cancelRequest(withError: error)
            completion(.failure(error))
        }

        func completeRequestWithItem(itemProvider: NSItemProvider) {
            let item = NSExtensionItem()
            item.attachments = [itemProvider]
            context.completeRequest(returningItems: [item]) { status in
                completion(.success(status))
            }
        }

        loadFilterListItem(forContentBlocker: cbIdentifier,
                          completion: handleResult(completeRequestWithItem, cancelRequestAndComplete))
    }

    // MARK: - Private methods

    private func loadFilterListItem(forContentBlocker cbIdentifier: ContentBlockerIdentifier,
                                    completion: @escaping (Result<NSItemProvider, Error>) -> Void) {
        let loadURL = filterListContainerURL.appendingPathComponent("\(cbIdentifier).json")
        completion(Result {
            guard FileManager.default.fileExists(atPath: loadURL.path) else {
                throw ABPKitSafariError.missingActiveFilterList
            }
            if let item = NSItemProvider(contentsOf: loadURL) {
                return item
            } else {
                throw ABPKitSafariError.failedToBuildFilterListItemProvider
            }

        })
    }
}
