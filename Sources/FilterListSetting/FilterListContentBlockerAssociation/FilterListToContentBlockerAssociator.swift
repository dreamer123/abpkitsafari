// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Component responsible for loading new rules from the filter list loader for a given rules source
/// and update the current active rules list for a given content blocker identifier
final class FilterListToContentBlockerAssociator: FilterListSetter {
    private let filterListLoader: FilterListLoader
    private let activeFilterListSaver: ActiveFilterListSaver

    /// Creates an instance
    /// - Parameters:
    ///   - rulesLoader: To be used to load rules for a particular rules source
    ///   - activeRulesSaver: To be used to save the loaded rules as active for a given content blocker id
    init(filterListLoader: FilterListLoader, activeFilterListSaver: ActiveFilterListSaver) {
        self.filterListLoader = filterListLoader
        self.activeFilterListSaver = activeFilterListSaver
    }

    func setFilterList(forSource source: FilterListSource,
                       toContentBlocker cbIdentifier: ContentBlockerIdentifier,
                       completion: @escaping SetCompletion) {

        func completeWithError(error: Error) {
            completion(.failure(error))
        }

        func saveLoadedFilterList() -> (FilterList) -> Void {
            return { [weak self] filterList in
                let activeFilterList = ActiveFilterList(filterList: filterList, cbIdentifier: cbIdentifier)
                self?.activeFilterListSaver.saveActiveFilterList(activeFilterList, completion: completion)
            }
        }

        filterListLoader.loadFilterList(forSource: source,
                                        completion: handleResult(saveLoadedFilterList(),
                                                                 completeWithError))
    }
}
