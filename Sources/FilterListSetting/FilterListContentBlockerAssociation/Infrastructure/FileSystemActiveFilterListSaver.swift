// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Saves the content blocker active filter list
final class FileSystemActiveFilterListSaver: ActiveFilterListSaver {
    private let containerURL: URL
    private let queue = DispatchQueue(label: "\(FileSystemActiveFilterListSaver.self).queue",
                                      qos: .userInitiated,
                                      attributes: .concurrent)

    /// Creates a new instance
    /// - Parameter activeFilterListsStoreURL: The container url where active filter lists need to be saved
    init(containerURL: URL) {
        self.containerURL = containerURL
    }

    func saveActiveFilterList(_ activeFilterList: ActiveFilterList,
                              completion: @escaping SaveCompletion) {
        let saveURL = containerURL.appendingPathComponent("\(activeFilterList.cbIdentifier).json")
        queue.async(flags: .barrier) {
            completion(Result {
                try activeFilterList.filterList.content.write(to: saveURL,
                                                              atomically: true,
                                                              encoding: .utf8)
            }.mapError(ABPKitSafariError.failedToCacheActiveFilterList))
        }
    }
}
