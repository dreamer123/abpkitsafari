// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import ABPKitCore

typealias FilterList = ABPKitCore.FilterList
typealias FilterListUpdateObserver = ABPKitCore.UpdateObserver<FilterListUpdateEvent>
typealias FilterListUpdater = ABPKitCore.FilterListUpdater<FilterListUpdateEvent>
typealias FilterListExpiredSourcesLoader = ABPKitCore.FilterListExpiredSourcesLoader
typealias CodableFilterListSource = ABPKitCore.CodableFilterListSource
typealias FilterListLoader = ABPKitCore.FilterListLoader
typealias RemoteSourceFilterListLoader = ABPKitCore.FilterListTypeLoader

// Forwarded ABPKitCore components, so clients do not have to import also ABPKitCore in order
// to make use of ABPKitSafari functionality.

/// Forwarded type from ABPKitCore. See [ABPKitCore.FilterListSource](https://eyeo.gitlab.io/sandbox/abpkitcore/Enums/FilterListSource.html)
public typealias FilterListSource = ABPKitCore.FilterListSource

/// Forwarded type from ABPKitCore. See [ABPKitCore.FilterListType](https://eyeo.gitlab.io/adblockplus/abpkitcore/Enums/FilterListType.html)
public typealias FilterListType = ABPKitCore.FilterListType

/// Forwarded type from ABPKitCore. See [ABPKitCore.FilterListAAVariant](https://eyeo.gitlab.io/adblockplus/abpkitcore/Enums/FilterListAAVariant.html)
public typealias FilterListAAVariant = ABPKitCore.FilterListAAVariant

/// Forwarded type from ABPKitCore. See [ABPKitCore.FilterListABPSource](https://eyeo.gitlab.io/adblockplus/abpkitcore/Enums/FilterListABPSource.html)
public typealias FilterListABPSource = ABPKitCore.FilterListABPSource

/// Forwarded type from ABPKitCore. See [ABPKitCore.AllowedDomain](https://eyeo.gitlab.io/adblockplus/abpkitcore/Typealiases.html#/s:10ABPKitCore13AllowedDomaina)
public typealias AllowedDomain = ABPKitCore.AllowedDomain

/// Forwarded type from ABPKitCore. See [ABPKitCore.AllowedDomains](https://eyeo.gitlab.io/adblockplus/abpkitcore/Typealiases.html#/s:10ABPKitCore14AllowedDomainsa)
public typealias AllowedDomains = ABPKitCore.AllowedDomains

/// Forwarded type from ABPKitCore. See [ABPKitCore.AllowedDomainsRepository](https://eyeo.gitlab.io/adblockplus/abpkitcore/Typealiases.html#/s:10ABPKitCore24AllowedDomainsRepositorya)
public typealias AllowedDomainsRepository = ABPKitCore.AllowedDomainsRepository

/// Contains the information about client's application
public struct App {
    let name: String
    let version: String

    /// Creates a new instance
    /// - Parameters:
    ///   - name: The name of client's application
    ///   - version: The version of client's application
    public init(name: String, version: String) {
        self.name = name
        self.version = version
    }
}

extension App {
    var toCoreApp: ABPKitCore.App {
        .init(name: name, version: version)
    }
}
