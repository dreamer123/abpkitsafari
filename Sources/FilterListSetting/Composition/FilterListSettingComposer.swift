// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation
import ABPKitCore

/// The URL pointing to the client app group
public typealias AppGroupURL = URL

/// Auto updater that will perform automatic updates for the current active filter lists for all of the active content blockers
public typealias ContentBlockerAutoUpdater = FilterListAutoUpdater<FilterListUpdateEvent>

/// The composition root for filter list setting features.
/// When integrating, most probably you would want to wrap this in a shared/singleton instance
/// so you don't perform the setup again and again.
/// Still, this composer works as expected if you create multiple instances of it.
public final class FilterListSettingComposer {

    // MARK: - Public component instances -
    /// Instance of FilterListSetter, to be used to set the filter lists on a content blocker extension.
    ///
    /// This composition provide the next features:
    /// - Load the filter list for the given source
    /// - Associate the loaded filter list with the given content blocker
    /// - Reload the extension for the given content blocker
    public private(set) lazy var filterListSetter: FilterListSetter = _filterListSetter

    /// Instance of SafariContentBlockingRequestHandler, to be used to handle the content blocking extension request.
    public private(set) lazy var contentBlockerRequestHandler = makeContentBlockerRequestHandler()

    /// Instance of [AllowedDomainsRepository](https://eyeo.gitlab.io/sandbox/abpkitcore/Typealiases.html#/s:10ABPKitCore24AllowedDomainsRepositorya), to be used for all AllowedDomains operation.
    public private(set) lazy var allowedDomainsRepository = makeAllowedDomainsRepository()

    /// Instance of ActiveSourcesLoader, which allows loading the current active sources.
    public private(set) lazy var activeSourcesLoader: ActiveSourcesLoader = localActiveSourcesLoader

    /// Instance of ContentBlockersReloader, which allows reloading all content content blockers with latest active source.
    public private(set) lazy var contentBlockersReloader = makeContentBlockersReloader()

    /// Get the instance of an filter list auto updater to be used to automagically update the active filter lists on all active content blockers.
    /// Filter lists that aren't used by any content blocker will not be updated
    ///
    /// Auto updater will check, with the given interval if active filter lists need updating. If there is something to update it will start the update process
    /// and notify accordingly.
    ///
    /// Auto updater uses an instance of FilterListSetter similar to one used for filterListSetter, thus auto update is actually calling for you the filterListSetter
    /// with the active filter list on a given content blocker if it needs updating. You are to expect the same behaviour as if you would have called the
    /// filterListSetter yourself.
    ///
    /// Auto update is a low priority operation, thus auto update will not fire if you are currently setting a filter list on any content blocker, or
    /// it auto update will be postponed if during auto update you fired a filter list setting request on any content blocker. This is needed to
    /// not interfere with user interaction, so filterListSetter is the primary way of setting a filter list on any content blocker
    public private(set) lazy var autoUpdater = makeContentBlockerAutoUpdater()

    // MARK: - Private component instances -
    private lazy var _filterListSetter = makeFilterListSetter()
    private lazy var localActiveSourcesLoader = makeLocalActiveSourcesLoader()
    private lazy var extensionReloader = makeExtensionReloader()
    private lazy var activeFilterListSaver = makeActiveFilterListSaver()

    // MARK: - Config properties -

    private let appGroupURL: AppGroupURL
    private let filterListManagementComposer: FilterListManagementComposer

    // MARK: - Initialiser -

    /// Sets up the composer with initial configuration.
    /// The configuration will be used further down the line when composing needed functionality.
    ///
    /// - Parameters:
    ///   - appInfo: Information about the client app, will be used for filter list download queries.
    ///   - appGroupURL: All of the downloaded data will be stored in the app group. Be sure to pass a correct
    ///                  app group url, otherwise the data will not be shared between main app and the extension.
    ///   - appBundle: The client application bundle to be used to load bundled filter lists.
    ///   - currentDate: Current system time. Used to timestamp the downloaded filter lists, also to determine if a filter list is expired.
    ///                  Has a default value set, however this parameter can be used to control the time in the framework - freeze, fast forward,
    ///                  travel to the past; overall, useful for testing purpose.
    public init(appInfo: App,
                appGroupURL: AppGroupURL,
                appBundle: Bundle,
                currentDate: @escaping () -> Date = Date.init) {

        let clientMetaData = ClientMetaData(addon: .currentAddonInfo,
                                            app: appInfo.toCoreApp,
                                            platform: .currentPlatformInfo)
        filterListManagementComposer = FilterListManagementComposer(clientMetaData: clientMetaData,
                                                                    clientAppBundle: appBundle,
                                                                    storageContainerURL: appGroupURL,
                                                                    systemTime: currentDate)
        self.appGroupURL = appGroupURL
    }

    // MARK: - Composer functions -

    func makeFilterListSetter() -> FilterListSetterWithExecutionStatus {
        // Create the filter list loader, which handles everything related to loading, caching, reloading a filter list
        let filterListLoader = filterListManagementComposer.filterListLoader
        return Self.makeFilterListSetter(filterListLoader: filterListLoader,
                                         activeFilterListSaver: activeFilterListSaver,
                                         activeSourcesSaver: localActiveSourcesLoader,
                                         extensionReloader: extensionReloader)
                                         .withExecutionStatus()
    }

    func makeContentBlockerRequestHandler() -> SafariContentBlockingRequestHandler {
        SafariContentBlockingRequestHandler(filterListContainerURL: appGroupURL)
    }

    func makeAllowedDomainsRepository() -> AllowedDomainsRepository {
        return filterListManagementComposer.allowedDomainsRepository
    }

    func makeContentBlockerAutoUpdater() -> ContentBlockerAutoUpdater {
        let shortCircuit = { [unowned self] in
            self._filterListSetter.isExecuting
        }

        let autoUpdateFilterListSetter = makeAutoUpdateFilterListSetter(shortCircuit: shortCircuit)
        let sourcesToUpdateProvider = makeSourcesToUpdateProvider()
        let updater = buildFilterListUpdater(sourcesToUpdateProvider: sourcesToUpdateProvider,
                                             filterListSetter: autoUpdateFilterListSetter,
                                             shortCircuit: shortCircuit)

        let autoUpdater = filterListManagementComposer.makeAutoUpdater(using: updater)
        autoUpdater.observeUpdates(autoUpdateLogger)
        return autoUpdater
    }

    /// Filter list setter to be used for auto update.
    /// It is different from one used by the client app to peform filter list setting, by being low priority filter list setter.
    /// By using shortcircuit mechanism, this filter list setter will not execute or abort exectution if primary filter list setter is executing
    func makeAutoUpdateFilterListSetter(shortCircuit: @escaping ShortCircuit) -> FilterListSetter {

        /// Loads a filter list always from remote server
        let remoteLoader = filterListManagementComposer.remoteFilterListLoader
        let filterListLoader = remoteLoader.asFilterListLoader().shortCircuited(shortCircuit)
        let activeFilterListSaver = self.activeFilterListSaver.shortCircuited(shortCircuit)
        let extensionReloader = self.extensionReloader.shortCircuited(shortCircuit)

        return FilterListToContentBlockerAssociator(filterListLoader: filterListLoader,
                                                    activeFilterListSaver: activeFilterListSaver)
               .withExtensionReload(using: extensionReloader)
    }

    func makeSourcesToUpdateProvider() -> SourcesToUpdateProvider {
        /// Loads all of the expired active sources
        let expiredSourcesLoader = filterListManagementComposer.filterListExpiredSourcesLoader
        /// Determines which content blockers need updating based on the expired sources
        return ActiveSourcesToUpdateProvider(expiredSourcesLoader: expiredSourcesLoader,
                                             activeSourcesLoader: activeSourcesLoader)
    }

    func makeLocalActiveSourcesLoader() -> LocalActiveSourcesLoader {
        let fileSystemActiveSourcesStore = FileSystemActiveSourcesStore(storeURL: activeSourcesStorage(container: appGroupURL)).withLogging()
        return LocalActiveSourcesLoader(store: fileSystemActiveSourcesStore)
    }

    func makeContentBlockersReloader() -> ContentBlockersReloader {
        return ContentBlockersReloader(activeSourcesLoader: activeSourcesLoader,
                                       filterListSetter: filterListSetter)
    }

    func makeExtensionReloader() -> ExtensionReloader {
        SafariManagerExtensionReloader().withLogging()
    }

    func makeActiveFilterListSaver() -> ActiveFilterListSaver {
        FileSystemActiveFilterListSaver(containerURL: appGroupURL).withLogging()
    }

    static func makeFilterListSetter(filterListLoader: FilterListLoader,
                                     activeFilterListSaver: ActiveFilterListSaver,
                                     activeSourcesSaver: ActiveSourceSaver,
                                     extensionReloader: ExtensionReloader) -> FilterListSetter {
        /// Associate the loaded filter list to the content blocker
        FilterListToContentBlockerAssociator(filterListLoader: filterListLoader,
                                             activeFilterListSaver: activeFilterListSaver)
            /// Save the source as active for the content blocker
            .withActiveSourceSave(to: activeSourcesSaver)
            /// Reload the extension
            .withExtensionReload(using: extensionReloader)
    }

    func activeSourcesStorage(container: URL) -> URL {
        container.appendingPathComponent("ABPKitSafari-ActiveSources.store")
    }
}

// MARK: - Composition Helpers -

private extension FilterListSetter {
    func withExtensionReload(using reloader: ExtensionReloader) -> FilterListSetter {
        FilterListSetterWithExtensionReload(decoratee: self, extensionReloader: reloader)
    }

    func withActiveSourceSave(to saver: ActiveSourceSaver) -> FilterListSetter {
        FilterListSetterWithActiveSourceSave(decoratee: self, activeSourceSaver: saver)
    }

    func withExecutionStatus() -> FilterListSetterWithExecutionStatus {
        FilterListSetterWithExecutionStatus(decoratee: self)
    }
}

extension RemoteSourceFilterListLoader {
    func asFilterListLoader() -> FilterListLoader {
        RemoteSourceToFilterListLoader(loader: self)
    }
}

extension ExtensionReloader {
    func withLogging() -> ExtensionReloader {
        ExtensionReloaderWithLogging(decoratee: self)
    }
}

extension ActiveFilterListSaver {
    func withLogging() -> ActiveFilterListSaver {
        ActiveFilterListSaverWithLogging(decoratee: self)
    }
}

extension ActiveSourcesStore {
    func withLogging() -> ActiveSourcesStore {
        ActiveSourcesStoreWithLogging(decoratee: self)
    }
}

extension ExtensionReloader {
    func shortCircuited(_ shortCircuit: @escaping ShortCircuit) -> ExtensionReloader {
        return ShortCircuitedExtensionReloader(decoratee: self, shortCircuit: shortCircuit)
    }
}

extension ActiveFilterListSaver {
    func shortCircuited(_ shortCircuit: @escaping ShortCircuit) -> ActiveFilterListSaver {
        return ShortCircuitedActiveFilterListSaver(decoratee: self, shortCircuit: shortCircuit)
    }
}

extension FilterListLoader {
    func shortCircuited(_ shortCircuit: @escaping ShortCircuit) -> FilterListLoader {
        return ShortCircuitedFilterListLoader(decoratee: self, shortCircuit: shortCircuit)
    }
}

/// Useful to transform a RemoteSource filter list loader to a FilterListSource loader
private final class RemoteSourceToFilterListLoader: FilterListLoader {

    struct BadSourceError: Error {}

    private let loader: RemoteSourceFilterListLoader

    init(loader: RemoteSourceFilterListLoader) {
        self.loader = loader
    }

    func loadFilterList(forSource source: FilterListSource, completion: @escaping LoadCompletion) {
        if case let .remote(remoteSource) = source {
            loader.loadFilterList(forSource: remoteSource,
                                  completion: completion)
        } else {
            completion(.failure(BadSourceError()))
        }
    }
}

private extension Addon {
    static var currentVersion: String {
        "0.0.1"
    }

    static var currentAddonInfo: Addon {
        .init(name: "safari", version: currentVersion)
    }
}

private extension Platform {
    private static let webkitID = "com.apple.WebKit"
    private static let webkitVersionKey = "CFBundleVersion"

    static var currentPlatformInfo: Platform {
        let webkit = Bundle(identifier: webkitID)
        let dict = webkit?.infoDictionary
        let version = (dict?[webkitVersionKey] as? String) ?? ""
        return .init(name: "webkit", version: version)
    }
}
