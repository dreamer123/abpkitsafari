// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Component responsible for saving the active source a given content blocker id
final class FilterListSetterWithActiveSourceSave: FilterListSetter {
    private let decoratee: FilterListSetter
    private let activeSourceSaver: ActiveSourceSaver

    /// Creates an instance
    /// - Parameters:
    ///   - decoratee: Setter to be decorated with new behaviour
    ///   - activeSourceSaver: To be used to save the active source if decoratee rules set succeeded
    init(decoratee: FilterListSetter,
         activeSourceSaver: ActiveSourceSaver) {
        self.decoratee = decoratee
        self.activeSourceSaver = activeSourceSaver
    }

    func setFilterList(forSource source: FilterListSource,
                       toContentBlocker cbIdentifier: ContentBlockerIdentifier,
                       completion: @escaping SetCompletion) {
        func completeWihFailure(_ error: Error) {
            completion(.failure(error))
        }

        func completeWithSuccess() {
            completion(.success)
        }

        func saveActiveSource() -> () -> Void {
            return { [weak self] in
                guard let self = self else { return }

                func ignoreSaveResult(_ result: ActiveSourceSaver.SaveResult) {}

                let sourceToSave = ContentBlockerActiveSource(source: source,
                                                              cbId: cbIdentifier)
                self.activeSourceSaver.saveActiveSource(sourceToSave,
                                                       completion: ignoreSaveResult)
                completeWithSuccess()
            }
        }

        decoratee.setFilterList(forSource: source,
                                toContentBlocker: cbIdentifier,
                                completion: handleResult(saveActiveSource(), completeWihFailure))
    }
}
