// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Decorates the filter list setter with extensio reload functionality
final class FilterListSetterWithExtensionReload: FilterListSetter  {
    private let decoratee: FilterListSetter
    private let extensionReloader: ExtensionReloader

    /// Creates a new instance
    /// - Parameters:
    ///   - decoratee: To be used to set the filter list
    ///   - extensionReloader: To be used to reload the extension if filter list set succeeded
    init(decoratee: FilterListSetter, extensionReloader: ExtensionReloader) {
        self.decoratee = decoratee
        self.extensionReloader = extensionReloader
    }

    func setFilterList(forSource source: FilterListSource,
                       toContentBlocker cbIdentifier: ContentBlockerIdentifier,
                       completion: @escaping SetCompletion) {
        func reloadContentBlocker() -> () -> Void {
            return { [weak self] in
                self?.extensionReloader.reloadContentBlocker(forId: cbIdentifier, completion: completion)
            }
        }

        func completeWithError(error: Error) {
            completion(.failure(error))
        }

        decoratee.setFilterList(forSource: source,
                                toContentBlocker: cbIdentifier,
                                completion: handleResult(reloadContentBlocker(), completeWithError))
    }
}
