/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

final class ShortCircuitedExtensionReloader: ExtensionReloader {
    private let decoratee: ExtensionReloader
    private let shortCircuit: ShortCircuit

    init(decoratee: ExtensionReloader, shortCircuit: @escaping ShortCircuit) {
        self.decoratee = decoratee
        self.shortCircuit = shortCircuit
    }

    func reloadContentBlocker(forId cbId: ContentBlockerIdentifier, completion: @escaping ReloadCompletion) {
        guard !shortCircuit() else {
            return completion(.failure(ABPKitSafariError.executionCancelled))
        }
        decoratee.reloadContentBlocker(forId: cbId, completion: completion)
    }
}
