/*
* This file is part of Adblock Plus <https://adblockplus.org/>,
* Copyright (C) 2006-present eyeo GmbH
*
* Adblock Plus is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Adblock Plus is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
*/

import Foundation

final class FilterListSetterWithExecutionStatus: FilterListSetter {

    // MARK: - Private properties

    private let decoratee: FilterListSetter
    private let serialDispatchQueue = DispatchQueue(label: "FilterListSetterWithExecutionStatus.queue")
    private var executionCount = 0

    // MARK: - Internal API

    var isExecuting: Bool {
        return executionCount > 0
    }

    init(decoratee: FilterListSetter) {
        self.decoratee = decoratee
    }

    func setFilterList(forSource source: FilterListSource,
                       toContentBlocker cbIdentifier: ContentBlockerIdentifier,
                       completion: @escaping SetCompletion) {
        serialDispatchQueue.sync {
            self.executionCount += 1
        }
        decoratee.setFilterList(forSource: source,
                                toContentBlocker: cbIdentifier) { [weak self] result in
                                    self?.serialDispatchQueue.sync {
                                        self?.executionCount -= 1
                                    }
                                    completion(result)
        }
    }
}
