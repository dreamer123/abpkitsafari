// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Events that will be fired when Auto update is happening
public enum FilterListUpdateEvent {
    /// Started to perform update for a content blocker extension
    case startedToUpdate(source: ContentBlockerSourceToUpdate)
    /// Completed to perform update for a content blocker extension, be it success or failure
    case completedToUpdate(source: ContentBlockerSourceToUpdate, result: Result<Void, Error>)
    /// Update for particular source was cancelled
    case cancelledUpdate(source: ContentBlockerSourceToUpdate)
}

extension FilterListUpdateEvent: Equatable {
    public static func == (lhs: FilterListUpdateEvent, rhs: FilterListUpdateEvent) -> Bool {
        switch (lhs, rhs) {
        case let (.startedToUpdate(lhsSource), .startedToUpdate(rhsSource)):
            return lhsSource == rhsSource
        case let (.completedToUpdate(lhsSource, lhsResult), .completedToUpdate(rhsSource, rhsResult)):
            guard lhsSource == rhsSource else {
                return false
            }
            switch (lhsResult, rhsResult) {
            case (.success, .success), (.failure, .failure):
                return true
            default:
                return false
            }
        case let (.cancelledUpdate(lhsSource), .cancelledUpdate(rhsSource)):
            return lhsSource == rhsSource
        default:
            return false
        }
    }
}

/// Builds the safari extension filter list updater that conforms to FilterListUpdater<FilterListUpdateEvent> contract
/// - Parameters:
///   - sourcesToUpdateProvider: The provider of sources that need update
///   - filterListSetter: The setter to be used to ask to update the filterlist for a given content blocker based on the active source
///   - shortCircuit: Meant to check if current update cycle does not need to start or that it needs to be stopped.
/// - Returns: A safari extension specific filter list updater
func buildFilterListUpdater(sourcesToUpdateProvider: SourcesToUpdateProvider,
                            filterListSetter: FilterListSetter,
                            shortCircuit: @escaping ShortCircuit) -> FilterListUpdater {
    // swiftlint:disable closure_body_length
    return { updateObserver in

        /// Make sure update cycle can proceed
        guard shortCircuit() == false else { return }

        func ignoreErrorPerformNoUpdates(_ error: Error) {}

        func notifyUpdateCompletedSuccessfully(_ source: ContentBlockerSourceToUpdate) -> () -> Void {
            return {
                guard shortCircuit() == false else {
                    return updateObserver(.cancelledUpdate(source: source))
                }
                updateObserver(.completedToUpdate(source: source, result: .success))
            }
        }

        func notifyUpdateCompletedWithFailure(_ source: ContentBlockerSourceToUpdate) -> (Error) -> Void {
            return { failure in
                guard shortCircuit() == false else {
                    return updateObserver(.cancelledUpdate(source: source))
                }
                updateObserver(.completedToUpdate(source: source,
                                                  result: .failure(failure)))
            }
        }

        func notifyStartedtoUpdate(_ source: ContentBlockerSourceToUpdate) {
            updateObserver(.startedToUpdate(source: source))
        }

        func performUpdates(_ sources: [ContentBlockerSourceToUpdate]) {
            sources.forEach { cbActiveSource in
                guard shortCircuit() == false else { return }
                notifyStartedtoUpdate(cbActiveSource)

                let notifyUpdateCompleted = handleResult(notifyUpdateCompletedSuccessfully(cbActiveSource),
                                                         notifyUpdateCompletedWithFailure(cbActiveSource))
                filterListSetter.setFilterList(forSource: .remote(cbActiveSource.source),
                                               toContentBlocker: cbActiveSource.cbId,
                                               completion: notifyUpdateCompleted)
            }
        }

        let onSourceToUpdateLoaded = handleResult(performUpdates, ignoreErrorPerformNoUpdates)
        sourcesToUpdateProvider.get(completion: onSourceToUpdateLoaded)
    }
}
