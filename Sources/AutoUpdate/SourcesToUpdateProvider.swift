// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Contains information about which content blocker and with which source it is needed to perform an update
public struct ContentBlockerSourceToUpdate: Equatable {

    /// The source to be used for update, only RemoteSource can be updated
    public let source: FilterListType

    /// The content blocker for which it is needed to update the rules based on the source
    public let cbId: ContentBlockerIdentifier
}

/// Describes the contract of providing the sources that need updating
protocol SourcesToUpdateProvider {
    typealias GetResult = Result<[ContentBlockerSourceToUpdate], Error>
    typealias GetCompletion = (GetResult) -> Void

    /// Gets the sources that need updating
    /// - Parameter completion: A completion of get operation
    func get(completion: @escaping GetCompletion)
}
