// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

final class ActiveSourcesStoreWithLogging: ActiveSourcesStore {
    private let decoratee: ActiveSourcesStore

    init(decoratee: ActiveSourcesStore) {
        self.decoratee = decoratee
    }

    func retrieve(completion: @escaping RetrieveCompletion) {
        logInfo(msg: "Retrieving active sources from cache")

        decoratee.retrieve { result in
            switch result {
            case let .success(activeSources):
                logInfo(msg: "Successfuly loaded active sources: %{public}@",
                        args: activeSources.logInfo)
            case let .failure(error):
                logError(msg: "Failed to load active sources, reason: %{public}@", args: error.debugDescription)
            }
            completion(result)
        }
    }

    func insert(_ sources: [ContentBlockerActiveSource], completion: @escaping InsertCompletion) {
        logInfo(msg: "Inserting active sources: %{public}@",
                args: sources.logInfo)
        decoratee.insert(sources) { result in
            switch result {
            case .success:
                logInfo(msg: "Successfuly inserted active sources")
            case let .failure(error):
                logError(msg: "Failed to insert active sources, reason: %{public}@", args: error.debugDescription)
            }
            completion(result)
        }
    }
}

extension ContentBlockerActiveSource {
    var logInfo: String {
        String(format: "source: %@, cbIdentifier: %@", source.logName, cbId)
    }
}

extension Array where Element == ContentBlockerActiveSource {
    var logInfo: String {
        map { $0.logInfo }.joined(separator: ", ")
    }
}
