// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

final class ActiveFilterListSaverWithLogging: ActiveFilterListSaver {
    private let decoratee: ActiveFilterListSaver

    init(decoratee: ActiveFilterListSaver) {
        self.decoratee = decoratee
    }

    func saveActiveFilterList(_ activeFilterList: ActiveFilterList, completion: @escaping SaveCompletion) {
        logInfo(msg: "Saving the active filter list for content blocker: %{public}@",
                args: activeFilterList.cbIdentifier)
        decoratee.saveActiveFilterList(activeFilterList) { result in
            switch result {
            case .success:
                logInfo(msg: "Successfuly saved active filter list for content blocker: %{public}@", args: activeFilterList.cbIdentifier)
            case let .failure(error):
                let message = String(format: "Failed to save active filter list for content blocker %@, reason: %@", activeFilterList.cbIdentifier, error.debugDescription)
                logError(msg: "%{public}@", args: message)
            }
            completion(result)
        }
    }
}
