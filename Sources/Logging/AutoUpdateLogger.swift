// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

func autoUpdateLogger(updateEvent: FilterListUpdateEvent) {
    switch updateEvent {
    case let .startedToUpdate(source):
        logInfo(msg: "Started to refresh filter list for %{public}@", args: source.logInfo)
    case let .completedToUpdate(source, result):
        switch result {
        case .success:
            logInfo(msg: "Did refresh filter list for %{public}@", args: source.logInfo)
        case let .failure(error):
            logInfo(msg: "Failed to refresh filter list for %{public}@",
                    args: source.logInfo + " error: " + error.debugDescription)
        }

    case let .cancelledUpdate(source):
        logInfo(msg: "Cancelled to refresh filter list from source %{public}@ on content blocker %{public}@", args: source.logInfo)
    }
}

extension ContentBlockerSourceToUpdate {
    var logInfo: String {
        String(format: "source: %@, cbIdentifier: %@", source.logName, cbId)
    }
}
