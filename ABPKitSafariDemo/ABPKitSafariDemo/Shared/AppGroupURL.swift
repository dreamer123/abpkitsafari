// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

// Crash the app intentionalyy if failed to have correct setup
// swiftlint:disable force_unwrapping
// swiftlint:disable force_try
extension URL {
    static var iosAppGroupURL: URL {
        FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.org.adblockplus.abpkitsafari-ios")!
    }

    static var macOSAppGroupURL: URL {
        FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.org.adblockplus.abpkitsafari-macOS")!
    }

    static var abpKitContainer: URL {
        let groupURL: URL
        #if os(macOS)
            groupURL = macOSAppGroupURL
        #else
            groupURL = iosAppGroupURL
        #endif
        let abpKitContainer = groupURL.appendingPathComponent("ABPKit")
        try! FileManager.default.createDirectory(atPath: abpKitContainer.path, withIntermediateDirectories: true, attributes: nil)
        return abpKitContainer
    }
}
