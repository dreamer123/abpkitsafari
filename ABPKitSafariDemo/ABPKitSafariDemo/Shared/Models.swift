// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

let iosContentBlockerId1 = "org.adblockplus.devbuilds.ABPKitSafariDemo-iOS.DemoContentBlocker1"
let iosContentBlockerId2 = "org.adblockplus.devbuilds.ABPKitSafariDemo-iOS.DemoContentBlocker2"
let macosContentBlockerId1 = "org.adblockplus.devbuilds.ABPKitSafariDemo-macOS.DemoContentBlocker1"
let macosContentBlockerId2 = "org.adblockplus.devbuilds.ABPKitSafariDemo-macOS.DemoContentBlocker2"

enum Filter: Int, CaseIterable {
    case easyList
    case easyListChina
    case easyListDutch
    case easyListGermany
    case easyListItaly
    case easyListSpain
    case easyListFrance
    case easyListRussiaUkraine

    var title: String {
        switch self {
        case .easyList:
            return "easyList"
        case .easyListChina:
            return "easyListChina"
        case .easyListDutch:
            return "easyListDutch"
        case .easyListGermany:
            return "easyListGermany"
        case .easyListItaly:
            return "easyListItaly"
        case .easyListSpain:
            return "easyListSpain"
        case .easyListFrance:
            return "easyListFrance"
        case .easyListRussiaUkraine:
            return "easyListRussiaUkraine"
        }
    }
}

enum LoadSource: Int, CaseIterable {
    case bundle
    case remote

    var title: String {
        switch self {
        case .bundle:
            return "Bundle"
        case .remote:
            return "Remote"
        }
    }
}

// A custom model to expres content blocking configuration for demo app
struct ContentBlockingData {
    var filter: Filter
    var loadSource: LoadSource
    var withAA: Bool
    let cbId: String
    let title: String
}
