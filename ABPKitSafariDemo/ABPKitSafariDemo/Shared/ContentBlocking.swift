// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation
import ABPKitSafari

var systemTime = Date()

class ContentBlocking {
    private(set) lazy var filterListSettingComposer = makeFilterListSettingComposer()

    /// It is not necessary for class handling the logic around filter list setter to be a singleton,
    /// This is just for the sake of example
    static let instance = ContentBlocking()

    var onContentBlockerUpdating: (_ cbId: String) -> Void = { _ in }
    var onContentBlockerUpdated: (_ cbId: String, _ isSuccess: Bool) -> Void = { _, _ in }

    var isAutoUpdateEnabled: Bool {
        filterListSettingComposer.autoUpdater.isRunning
    }

    private init() {
        filterListSettingComposer.autoUpdater.observeUpdates { event in
            DispatchQueue.main.async {
                switch event {
                case let .startedToUpdate(source: source):
                    self.onContentBlockerUpdating(source.cbId)
                case let .completedToUpdate(source: source, result: result):
                    self.onContentBlockerUpdated(source.cbId, result.isSuccess)
                case .cancelledUpdate:
                    // Not yet handled
                    break
                }
            }
        }
    }

    func updateContentBlocker(with contentBlockingData: ContentBlockingData) {
        // Map custom data to ABPKitSafari Filter list source.
        // This is an example on how clients can have custom logic on how to identify a filter list,
        // and then map it to expected filter list source. Thus clients are not forced to design
        // the client app based on how ABPKitSafari works.
        let source: FilterListSource = contentBlockingData.filterListSource
        onContentBlockerUpdating(contentBlockingData.cbId)
        filterListSettingComposer.filterListSetter.setFilterList(forSource: source,
                                                                 toContentBlocker: contentBlockingData.cbId,
                                                                 completion: { result in
                                                                    DispatchQueue.main.async {
                                                                        self.onContentBlockerUpdated(contentBlockingData.cbId, result.isSuccess)
                                                                    }
        })
    }

    func reloadContentBlockers(completion: @escaping () -> Void) {
        filterListSettingComposer.contentBlockersReloader.reloadContentBlockers { event in
            DispatchQueue.main.async {
                switch event {
                case let .startedReloading(activeSources: sources):
                    sources.forEach { self.onContentBlockerUpdating($0.cbId) }
                case let .finishedReloading(result: result):
                    if case let .success(reloadResults) = result {
                        reloadResults.forEach {
                            self.onContentBlockerUpdated($0.activeSource.cbId, $0.result.isSuccess)
                        }
                    }
                }
                completion()
            }
        }
    }

    @objc
    func setAutoUpdateStatus(isEnabled: Bool) {
        if isEnabled {
            filterListSettingComposer.autoUpdater.stopAutoUpdate()
            filterListSettingComposer.autoUpdater.startAutoUpdate(withCheckInterval: 3.0)
        } else {
            filterListSettingComposer.autoUpdater.stopAutoUpdate()
        }
    }

    func getContentBlockingData(completion: @escaping ([ContentBlockingData]) -> Void) {
        filterListSettingComposer.activeSourcesLoader.loadActiveSources { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }

                switch result {
                case let .success(activeSources):
                    let cbData = self.cbIds().enumerated().map { index, id in
                        self.makeContentBlockerData(forId: id,
                                                    title: "Content blocker extension #\(index+1)",
                            using: activeSources)
                    }
                    completion(cbData)
                case .failure:
                    completion(self.defaultContentBlockingData())
                }
            }
        }
    }

    func makeContentBlockerData(forId id: String,
                                title: String,
                                using activeSources: [ContentBlockerActiveSource]) -> ContentBlockingData {
        guard let cb1 = activeSources.first(where: { $0.cbId == id }) else {
            return ContentBlockingData(filter: .easyList,
                                       loadSource: .bundle,
                                       withAA: false,
                                       cbId: id,
                                       title: title)
        }
        let filterCBData = cb1.source.contentBlockingData
        return ContentBlockingData(filter: filterCBData.filter,
                                   loadSource: filterCBData.loadSource,
                                   withAA: filterCBData.withAA,
                                   cbId: id,
                                   title: title)
    }

    private func cbIds() -> [String] {
        #if os(macOS)
            return [macosContentBlockerId1, macosContentBlockerId2]
        #else
            return [iosContentBlockerId1, iosContentBlockerId2]
        #endif
    }

    private func defaultContentBlockingData() -> [ContentBlockingData] {
        [ContentBlockingData(filter: .easyList,
                             loadSource: .bundle,
                             withAA: false,
                             cbId: cbIds()[0],
                             title: "Content blocker extension #1"),
         ContentBlockingData(filter: .easyList,
                             loadSource: .bundle,
                             withAA: false,
                             cbId: cbIds()[1],
                             title: "Content blocker extension #2")]
    }

    private func makeFilterListSettingComposer() -> FilterListSettingComposer {
        let app = App(name: "ABPKitSafariDemo",
                      version: "0.0.1")
        let bundle = Bundle(for: ContentBlocking.self)
        let container = URL.abpKitContainer

        return FilterListSettingComposer(appInfo: app,
                                         appGroupURL: container,
                                         appBundle: bundle,
                                         currentDate: { systemTime })
    }
}

extension Result {
    var isSuccess: Bool {
        if case .success = self {
            return true
        }
        return false
    }
}
// MARK: - ABPKitSafari <-> ABPKitSafariDemo domain mapping

extension ContentBlockingData {
    var filterListSource: FilterListSource {
        let filterListType: FilterListType
        if withAA {
            filterListType = .abp(.withAA(filter.abpSource))
        } else {
            filterListType = .abp(.withoutAA(filter.abpSource))
        }

        switch loadSource {
        case .bundle:
            return .bundled(filterListType)
        case .remote:
            return .remote(filterListType)
        }
    }
}

extension Filter {
    var abpSource: FilterListABPSource {
        switch self {
        case .easyList:
            return .easyList
        case .easyListChina:
            return .easyListChina
        case .easyListDutch:
            return .easyListDutch
        case .easyListGermany:
            return .easyListGermany
        case .easyListItaly:
            return .easyListItaly
        case .easyListSpain:
            return .easyListSpain
        case .easyListFrance:
            return .easyListFrance
        case .easyListRussiaUkraine:
            return .easyListRussiaUkraine
        }
    }
}

extension ContentBlockerActiveSource {
    var contentBlockingData: ContentBlockingData {
        let filterCBData = source.contentBlockingData

        return ContentBlockingData(filter: filterCBData.filter,
                                   loadSource: filterCBData.loadSource,
                                   withAA: filterCBData.withAA,
                                   cbId: cbId,
                                   title: "")
    }
}

extension FilterListSource {
    var contentBlockingData: (filter: Filter, loadSource: LoadSource, withAA: Bool) {
        var cbData: (filter: Filter, loadSource: LoadSource, withAA: Bool) = (.easyList, .bundle, false)

        let filterListType: FilterListType

        switch self {
        case let .bundled(source):
            cbData.loadSource = .bundle
            filterListType = source
        case let .remote(source):
            filterListType = source
            cbData.loadSource = .remote
        }

        switch filterListType {
        case let .abp(aaVariant):
            switch aaVariant {
            case let .withAA(source):
                cbData.withAA = true
                cbData.filter = source.filter
            case let .withoutAA(source):
                cbData.withAA = false
                cbData.filter = source.filter
            }
        case let .custom(path):
            // not yet handled
            break
        }

        return cbData
    }
}

extension FilterListABPSource {
    var filter: Filter {
        switch self {
        case .easyList:
            return .easyList
        // To update below when available
        case .easyListChina:
            return .easyListChina
        case .easyListDutch:
            return .easyListDutch
        case .easyListGermany:
            return .easyListGermany
        case .easyListItaly:
            return .easyListItaly
        case .easyListSpain:
            return .easyListSpain
        case .easyListFrance:
            return .easyListFrance
        case .easyListRussiaUkraine:
            return .easyListRussiaUkraine
        }
    }
}
