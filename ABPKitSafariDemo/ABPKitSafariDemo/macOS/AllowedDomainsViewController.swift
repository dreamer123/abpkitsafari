/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa

class AllowedDomainsViewControler: NSViewController {
    let allowedDomainsRepository = ContentBlocking.instance.filterListSettingComposer.allowedDomainsRepository

    @IBOutlet weak var allowedlistTitle: NSTextFieldCell!
    @IBOutlet weak var urlTextField: NSTextField!
    @IBOutlet weak var addWebsiteButton: NSButton!
    @IBOutlet weak var allowedlistCollectionView: NSCollectionView!

    private var allowedDomains = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        let item = NSUserInterfaceItemIdentifier(rawValue: "Item")
        allowedlistCollectionView.register(NSNib(nibNamed: "AllowedlistItem", bundle: nil), forItemWithIdentifier: item)
        allowedDomainsRepository.loadDomains(completion: refreshAllowedDomains())
    }

    override func viewWillDisappear() {
        super.viewWillDisappear()

        ContentBlocking.instance.reloadContentBlockers {}
    }

    func removeFromAllowedList(domain: String) {
        allowedDomainsRepository.removeDomains([domain], completion: refreshAllowedDomains())
    }

    @IBAction func urlTextFieldEntered(_ sender: NSTextField) {
        let hostName = sender.stringValue
        guard !hostName.isEmpty else {
            return
        }

        allowedDomainsRepository.addDomains([hostName], completion: refreshAllowedDomains())
        sender.stringValue = ""
    }

    @IBAction func addWebsiteToAllowedDomains(_ sender: NSButton) {
        urlTextFieldEntered(urlTextField)
    }

    @objc
    func updateAllowedDomains() {
        allowedlistCollectionView.reloadData()
    }

    private func refreshAllowedDomains() -> (Result<[String], Error>) -> Void {
        return { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case let .success(domains):
                    self?.allowedDomains = domains
                    self?.allowedlistCollectionView.reloadData()
                case .failure:
                    break
                }
            }
        }
    }
}

// MARK: - Extends ViewController to act as data source for the whitelist NSCollectionView
extension AllowedDomainsViewControler: NSCollectionViewDataSource {

    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return allowedDomains.count
    }

    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let allowedDomainItem = NSUserInterfaceItemIdentifier(rawValue: "Item")

        let item = collectionView.makeItem(withIdentifier: allowedDomainItem, for: indexPath) as? AllowedlistItem
        item?.textField?.stringValue = allowedDomains[indexPath.item]

        return item ?? NSCollectionViewItem()
    }
}

extension AllowedDomainsViewControler: NSCollectionViewDelegate {
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        collectionView.deselectItems(at: indexPaths)
        let index = indexPaths.first?.dropFirst()[0] ?? 0
        let domain = allowedDomains[index]
        let alert = NSAlert()
        alert.messageText = "Remove \(domain)?"
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "Yes")

        let selection = alert.runModal()

        if selection.rawValue != 1000 {
            removeFromAllowedList(domain: domain)
        }

    }
}
