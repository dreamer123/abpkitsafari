/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices

class HomeViewController: NSViewController {

    @IBOutlet weak var generalContainerView: NSView!
    @IBOutlet weak var allowedDomainsContainerView: NSView!
    @IBOutlet weak var helpContainerView: NSView!
    private var sidebarItems = ["ContentBlockers", "Allowed domains", "Settings"]
}

// MARK: - Extends ViewController to act as data source for the sidebar NSCollectionView
extension HomeViewController: NSCollectionViewDataSource {

    static let sidebarItem = "SidebarItem"

    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return sidebarItems.count
    }

    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: HomeViewController.sidebarItem), for: indexPath)
        item.textField?.stringValue = sidebarItems[indexPath.item]

        return item
    }
}

// MARK: - Extends ViewController to act as delegate for the sidebar NSCollectionView
extension HomeViewController: NSCollectionViewDelegate {
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        let index = indexPaths.first?.dropFirst()[0] ?? 0
        switch index {
        case 0:
            // General Tab Clicked
            generalContainerView.isHidden = false
            allowedDomainsContainerView.isHidden = true
            helpContainerView.isHidden = true
        case 1:
            generalContainerView.isHidden = true
            allowedDomainsContainerView.isHidden = false
            helpContainerView.isHidden = true
        case 2:
            generalContainerView.isHidden = true
            allowedDomainsContainerView.isHidden = true
            helpContainerView.isHidden = false
        default:
            break
        }
    }
}
