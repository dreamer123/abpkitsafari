// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Cocoa

class SettingsViewController: NSViewController {

    @IBOutlet weak var autoUpdateEnabledCheckBox: NSButton!
    @IBOutlet weak var datePicker: NSDatePicker!

    override func viewDidLoad() {
        super.viewDidLoad()

        autoUpdateEnabledCheckBox.state = ContentBlocking.instance.isAutoUpdateEnabled ? .on : .off
        datePicker.dateValue = systemTime
    }

    @IBAction func systemTimeChanged(_ sender: NSDatePicker) {
        systemTime = datePicker.dateValue
    }

    @IBAction func clearCacheData(_ sender: Any) {
        FileManager.default.clearCache()
        let alert = NSAlert()
        alert.messageText = "Removed all cached data!"
        alert.runModal()
    }

    @IBAction func autoUpdateCheckChanged(_ sender: NSButton) {
        ContentBlocking.instance.setAutoUpdateStatus(isEnabled: sender.state == .on)
    }
}
