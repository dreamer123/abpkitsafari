/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices

class GeneralViewController: NSViewController, NSMenuDelegate {
    private let contentBlockingHandler = ContentBlocking.instance

    @IBOutlet weak var generalTitle: NSTextFieldCell!
    @IBOutlet weak var loadSourceButton: NSPopUpButton!
    @IBOutlet weak var filterListButton: NSPopUpButton!
    @IBOutlet weak var acceptableAdsCheckbox: NSButton!

    @IBOutlet weak var targetContentBlocker: NSPopUpButton!
    @IBOutlet weak var applyButton: NSButton!

    @IBOutlet weak var loadingIndicator: NSProgressIndicator!

    private var contentBlockingData = [ContentBlockingData]()

    private var currentContentBlockerIdx: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        loadingIndicator.isHidden = true
        loadSourceButton.isEnabled = true
        filterListButton.isEnabled = true
        targetContentBlocker.isEnabled = true

        populateLoadSourceButton()
        populateFilterListButton()

        ContentBlocking.instance.getContentBlockingData { data in
            self.contentBlockingData = data
            self.populateTargetContentBlockerButton()
            self.updateContent()
        }

        ContentBlocking.instance.onContentBlockerUpdating = { _ in
            self.targetContentBlocker.isEnabled = false
            self.loadingIndicator.isHidden = false
            self.loadingIndicator.startAnimation(nil)
            self.applyButton.isEnabled = false
            self.filterListButton.isEnabled = false
            self.acceptableAdsCheckbox.isEnabled = false
            self.loadSourceButton.isEnabled = false
        }

        ContentBlocking.instance.onContentBlockerUpdated = { cbId, isSuccess in
            self.targetContentBlocker.isEnabled = true
            self.loadingIndicator.isHidden = true
            self.loadingIndicator.stopAnimation(nil)
            self.applyButton.isEnabled = true
            self.filterListButton.isEnabled = true
            self.acceptableAdsCheckbox.isEnabled = true
            self.loadSourceButton.isEnabled = true

            guard let cbData = self.contentBlockingData.first(where: { $0.cbId == cbId }) else {
                return
            }

            let alert = NSAlert()
            alert.informativeText = "Updating \(cbData.title)"

            alert.messageText = isSuccess ? "Succeeded" : "Failed"
            alert.runModal()
        }
    }

    private func updateContent() {
        let activeContentBlockingData = contentBlockingData[currentContentBlockerIdx]
        acceptableAdsCheckbox.state = activeContentBlockingData.withAA ? .on : .off
        loadSourceButton.selectItem(withTag: activeContentBlockingData.loadSource.rawValue)
        filterListButton.selectItem(withTag: activeContentBlockingData.filter.rawValue)
    }

    /// Changes enabled state of checkbox, popup button and adds an animation spinner.
    /// This should be used while loading blocklists. Such as when acceptable ads button is toggled.
    /// Always called on main thread.
    ///
    /// - enabled: If passed in true, this will enable the UI element. False, will disable them.
    private func changeAcceptableAdsCheckboxState(enabled: Bool) {
        DispatchQueue.main.async {
            switch enabled {
            case true:
                self.acceptableAdsCheckbox.isEnabled = true
                self.loadSourceButton.isEnabled = true
            case false:
                self.acceptableAdsCheckbox.isEnabled = false
                self.loadSourceButton.isEnabled = false
            }
        }
    }

    private func populateLoadSourceButton() {
        for source in LoadSource.allCases {
            loadSourceButton.addItem(withTitle: source.title)
            let menuItemAttributedString = NSAttributedString(string: source.title)
            let menuItem = loadSourceButton.item(at: source.rawValue)
            menuItem?.attributedTitle = menuItemAttributedString
            menuItem?.tag = source.rawValue
        }
    }

    private func populateFilterListButton() {
        for filterList in Filter.allCases {
            filterListButton.addItem(withTitle: filterList.title)
            let menuItemAttributedString = NSAttributedString(string: filterList.title)
            let menuItem = filterListButton.item(at: filterList.rawValue)
            menuItem?.attributedTitle = menuItemAttributedString
            menuItem?.tag = filterList.rawValue
        }
    }

    private func populateTargetContentBlockerButton() {
        for (index, name) in contentBlockingData.map({ $0.title }).enumerated() {
            targetContentBlocker.addItem(withTitle: name)
            let menuItemAttributedString = NSAttributedString(string: name)
            let menuItem = targetContentBlocker.item(at: index)
            menuItem?.attributedTitle = menuItemAttributedString
            menuItem?.tag = index
        }
    }

    @IBAction func applyButtonTapped(_ sender: Any) {
        guard let targetIdx = targetContentBlocker.selectedItem?.tag,
            let filterId = filterListButton.selectedItem?.tag,
            let loadSourceId = loadSourceButton.selectedItem?.tag,
            let filter = Filter(rawValue: filterId),
            let loadSource = LoadSource(rawValue: loadSourceId) else {
                return
        }

//        switch loadSource {
//        case .aBPBundle, .aBPRemote:
//            guard  let filterId = filterListButton.selectedItem?.tag,
//                   let filter = Filter(rawValue: filterId) else {
//                return
//            }
//            // disable path filed
//            filterListButton.isEnabled = false
//        case .customBundle, .customRemote:
//            guard !customPathEntry.stringValue.isEmpty else { return }
//
//            // disable filter list
//            filterListButton.isEnabled = false
//        }

        let newContentBlockingData = ContentBlockingData(filter: filter,
                                                         loadSource: loadSource,
                                                         withAA: acceptableAdsCheckbox.state == .on,
                                                         cbId: contentBlockingData[targetIdx].cbId,
                                                         title: contentBlockingData[targetIdx].title)
        contentBlockingData[targetIdx] = newContentBlockingData
        contentBlockingHandler.updateContentBlocker(with: newContentBlockingData)
    }

    @IBAction func loadSourceChanged(_ sender: NSPopUpButton) {
//        guard let loadSourceIdx = sender.selectedItem?.tag,
//              let loadSource = LoadSource(rawValue: loadSourceIdx) else {
//            return
//        }
//
////        switch loadSource {
////        case .aBPBundle, .aBPRemote:
////            // disable path filed
////            filterListButton.isEnabled = false
////        case .customBundle, .customRemote:
////            // disable filter list
////            filterListButton.isEnabled = false
////        case .remote:
////        default:
////            <#code#>
////        }
    }

    @IBAction func targetContentBlockerChanged(_ sender: NSPopUpButton) {
        guard let contentBlockerIdx = sender.selectedItem?.tag else {
            return
        }
        currentContentBlockerIdx = contentBlockerIdx
        updateContent()
    }
}
