// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import UIKit

class ViewController: UITableViewController {

    lazy var contentBlockingHandler = ContentBlocking.instance

    private var contentBlockingData = [ContentBlockingData]()

    lazy var picker: Picker = {
        let picker = Picker()
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect(x: 0.0,
                              y: UIScreen.main.bounds.size.height - 300,
                              width: UIScreen.main.bounds.size.width,
                              height: 300)

        return picker
    }()

    lazy var toolbar: UIToolbar = {
        let toolBar = UIToolbar(frame: CGRect(x: 0.0,
                                              y: UIScreen.main.bounds.size.height - 300,
                                              width: UIScreen.main.bounds.size.width,
                                              height: 50))
        toolBar.barStyle = .default
        toolBar.items = [UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]

        return toolBar
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        title = "ABPKitSafari Demo"

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Settings", style: .plain, target: self, action: #selector(showSettings))

        self.picker.isHidden = true
        self.toolbar.isHidden = true
        self.view.addSubview(picker)
        self.view.addSubview(toolbar)

        ContentBlocking.instance.getContentBlockingData { [weak self] data in
            self?.contentBlockingData = data
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }

        ContentBlocking.instance.onContentBlockerUpdating = { cbId in
            guard let indexOfCB = self.contentBlockingData.firstIndex(where: { $0.cbId == cbId }) else { return }
            let cell = self.tableView.cellForRow(at: IndexPath(row: indexOfCB, section: 0)) as? ContentBlockerTableViewCell
            cell?.setUpdatingState()
        }

        ContentBlocking.instance.onContentBlockerUpdated = { cbId, _ in
            guard let indexOfCB = self.contentBlockingData.firstIndex(where: { $0.cbId == cbId }) else { return }
            let cell = self.tableView.cellForRow(at: IndexPath(row: indexOfCB, section: 0)) as? ContentBlockerTableViewCell
            cell?.setFinishedUpdatingState()
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentBlockingData.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ContentBlockerTableViewCell()

        let cbData = contentBlockingData[indexPath.row]
        let contentBlockerTitle = cbData.title
        cell.loadedFromButton.setTitle(cbData.loadSource.title,
                                       for: .normal)
        cell.filterListNameButton.setTitle(cbData.filter.title,
                                           for: .normal)
        cell.extensionNameLabel.text = contentBlockerTitle
        cell.aaSwitch.isOn = cbData.withAA

        cell.onLoadedFromTapped = { [weak self] in
            let options = LoadSource.allCases
            let optionTitles = options.map { $0.title }
            self?.picker.setItems(optionTitles)
            self?.showPicker(withSelectedRow: cbData.loadSource.rawValue)
            self?.picker.onItemSelected = { idx in
                cell.loadedFromButton.setTitle(optionTitles[idx], for: .normal)
                self?.contentBlockingData[indexPath.row].loadSource = options[idx]
            }
        }

        cell.onFilterListTapped = { [weak self] in
            let options = Filter.allCases
            let optionTitles = options.map { $0.title }

            self?.picker.setItems(optionTitles)
            self?.showPicker(withSelectedRow: cbData.filter.rawValue)

            self?.picker.onItemSelected = { idx in
                cell.filterListNameButton.setTitle(optionTitles[idx], for: .normal)
                self?.contentBlockingData[indexPath.row].filter = options[idx]
            }
        }

        cell.onAASwitched = { [weak self] isOn in
            self?.contentBlockingData[indexPath.row].withAA = isOn
        }

        cell.onUpdateTapped = { [weak self] in
            guard let self = self else { return }
            self.contentBlockingHandler.updateContentBlocker(with: self.contentBlockingData[indexPath.row])
        }

        return cell
    }

    @objc
    func onDoneButtonTapped() {
        self.picker.isHidden = true
        self.toolbar.isHidden = true
    }

    @objc
    func showSettings() {
        self.navigationController?.pushViewController(SettingsViewController(), animated: true)
    }

    private func showPicker(withSelectedRow selectedRow: Int) {
        picker.selectRow(selectedRow, inComponent: 0, animated: false)
        picker.isHidden = false
        toolbar.isHidden = false
    }

    private func showDidUpdateContentBlocker(name: String) {
        presentAlert(withMessage: "Successfully updated \(name)")
    }

    private func showFailedToUpdateContentBlocker(name: String) {
        presentAlert(withMessage: "Failed to update \(name)")
    }

    private func presentAlert(withMessage message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(.init(title: "OK", style: .default, handler: nil))
        present(alert, animated: true)
    }
}
