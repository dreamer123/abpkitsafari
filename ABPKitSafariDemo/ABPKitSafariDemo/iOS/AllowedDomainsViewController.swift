// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import UIKit
import ABPKitSafari

class AllowedDomainsViewController: UITableViewController {
    private let domainsRepo = ContentBlocking.instance.filterListSettingComposer.allowedDomainsRepository

    private var domains = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Allowed domains"
        tableView.backgroundColor = .white

        domainsRepo.loadDomains(completion: reloadData())

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addDomain))
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // Reload content blockers once user made all the changes.
        ContentBlocking.instance.reloadContentBlockers { }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        domains.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text = domains[indexPath.row]

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showDeleteAllowedDomainAlert(domain: domains[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    private func showDeleteAllowedDomainAlert(domain: String) {
        let alertController = UIAlertController(title: domain, message: nil, preferredStyle: .alert)

        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { _ in
            self.domainsRepo.removeDomains([domain], completion: self.reloadData())
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in }

        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)

        present(alertController, animated: true)
    }

    @objc
    private func addDomain() {
        let alertController = UIAlertController(title: "Add website",
                                                message: "Type in a valid URL to add any website to your whitelist.",
                                                preferredStyle: .alert)

        alertController.addTextField { textField in
            textField.placeholder = "Website URL"
            textField.isSecureTextEntry = false
            textField.keyboardType = .URL
        }

        let addAction = UIAlertAction(title: "Add", style: .default) { _ in
            guard let newDomain = alertController.textFields?.first?.text,
                !newDomain.isEmpty,
                !self.domains.contains(newDomain) else {
                    return
            }

            self.domainsRepo.addDomains([newDomain], completion: self.reloadData())
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in }

        alertController.addAction(cancelAction)
        alertController.addAction(addAction)

        present(alertController, animated: true, completion: nil)
    }

    private func reloadData() -> (Result<AllowedDomains, Error>) -> Void {
        return { [weak self] result in
            self?.domains = (try? result.get()) ?? []
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
}
