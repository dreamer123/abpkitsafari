// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import UIKit

class ContentBlockerTableViewCell: UITableViewCell {
    lazy var filterListNameButton: UIButton = makeFilterListNameButton()
    lazy var loadedFromButton: UIButton = makeLoadedFromButton()
    lazy var aaSwitch: UISwitch = makeAASwitch()
    lazy var extensionNameLabel = makeExtensionNameLabel()

    private lazy var mainContainer = makeMainContainer()
    private lazy var updateButton = makeUpdateButton()
    private lazy var loadingIndicator = UIActivityIndicatorView()

    var onFilterListTapped: () -> Void = {}
    var onLoadedFromTapped: () -> Void = {}
    var onAASwitched: (Bool) -> Void = { _ in }
    var onUpdateTapped: () -> Void = {}

    init() {
        super.init(style: .default, reuseIdentifier: nil)
        setupLayout()
        selectionStyle = .none
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setUpdatingState() {
        filterListNameButton.isEnabled = false
        loadedFromButton.isEnabled = false
        aaSwitch.isEnabled = false
        updateButton.isHidden = true
        loadingIndicator.isHidden = false
        mainContainer.removeArrangedSubview(updateButton)
        mainContainer.addArrangedSubview(loadingIndicator)
        loadingIndicator.startAnimating()
    }

    func setFinishedUpdatingState() {
        filterListNameButton.isEnabled = true
        loadedFromButton.isEnabled = true
        aaSwitch.isEnabled = true
        loadingIndicator.stopAnimating()
        loadingIndicator.isHidden = true
        updateButton.isHidden = false
        mainContainer.removeArrangedSubview(loadingIndicator)
        mainContainer.addArrangedSubview(updateButton)
    }

    // MARK: - Layout -

    private func setupLayout() {
        contentView.addSubview(mainContainer)
        mainContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        mainContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
        mainContainer.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16).isActive = true
        mainContainer.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16).isActive = true
    }

    private func makeMainContainer() -> UIStackView {
        let filterListNameLabel = UILabel()
        filterListNameLabel.text = "Filter list name:"
        filterListNameLabel.textAlignment = .left
        let sourcedFromLabel = UILabel()
        sourcedFromLabel.text = "Loaded from:"
        sourcedFromLabel.textAlignment = .left
        let isWithAcceptableAdsLabel = UILabel()
        isWithAcceptableAdsLabel.text = "AA enabled:"
        isWithAcceptableAdsLabel.textAlignment = .left

        let namesStackView = UIStackView(arrangedSubviews: [filterListNameLabel, sourcedFromLabel, isWithAcceptableAdsLabel])
        namesStackView.axis = .vertical
        namesStackView.alignment = .leading
        namesStackView.spacing = 10
        namesStackView.distribution = .fillEqually

        let optionsStackView = UIStackView(arrangedSubviews: [filterListNameButton, loadedFromButton, aaSwitch])
        optionsStackView.axis = .vertical
        optionsStackView.alignment = .leading
        optionsStackView.spacing = 10
        optionsStackView.distribution = .fillEqually

        let configurationStackView = UIStackView(arrangedSubviews: [namesStackView, optionsStackView])
        configurationStackView.axis = .horizontal
        configurationStackView.distribution = .fillEqually
        configurationStackView.alignment = .fill

        let mainStackView = UIStackView(arrangedSubviews: [extensionNameLabel, configurationStackView, updateButton])
        mainStackView.axis = .vertical
        mainStackView.distribution = .fillProportionally
        mainStackView.alignment = .fill
        mainStackView.spacing = 20
        mainStackView.translatesAutoresizingMaskIntoConstraints = false

        return mainStackView
    }

    // MARK: - Factory methods -
    private func makeLabel(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.textAlignment = .left

        return label
    }

    private func makeExtensionNameLabel() -> UILabel {
        let extensionNameLabel = UILabel()
        extensionNameLabel.font = .boldSystemFont(ofSize: 20)

        return extensionNameLabel
    }

    private func makeLoadedFromButton() -> UIButton {
        makeBtn(selector: #selector(loadedFromButtonTapped))
    }

    private func makeFilterListNameButton() -> UIButton {
        makeBtn(selector: #selector(filterListNameButtonTapped))
    }

    private func makeUpdateButton() -> UIButton {
        let btn = makeBtn(selector: #selector(updateButtonTapped))
        btn.setTitle("Update", for: .normal)

        return btn
    }

    private func makeBtn(selector: Selector) -> UIButton {
        let btn = UIButton(type: .system)
        btn.setTitleColor(.black, for: .normal)
        btn.backgroundColor = .systemGray4
        btn.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn.layer.cornerRadius = 5

        btn.addTarget(self, action: selector, for: .touchUpInside)
        return btn
    }

    private func makeAASwitch() -> UISwitch {
        let aaSwitch = UISwitch()
        aaSwitch.addTarget(self, action: #selector(aaOptionChanged(withSender:)), for: .valueChanged)
        return aaSwitch
    }

    @objc
    func filterListNameButtonTapped() {
        onFilterListTapped()
    }

    @objc
    func loadedFromButtonTapped() {
        onLoadedFromTapped()
    }

    @objc
    func aaOptionChanged(withSender sender: UISwitch) {
        onAASwitched(sender.isOn)
    }

    @objc
    func updateButtonTapped() {
        onUpdateTapped()
    }
}
