// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import UIKit

class SettingsViewController: UITableViewController {
    enum Row: Int, CaseIterable {
        case allowedDomains
        case systemTime
        case autoUpdateEnabled
        case clearAllCache

        var title: String {
            switch self {
            case .allowedDomains:
                return "Allowed Domains"
            case .systemTime:
                return "System Time"
            case .autoUpdateEnabled:
                return "Auto Update Enabled"
            case .clearAllCache:
                return "Clear All Cache Data"
            }
        }
    }

    lazy var datePicker = UIDatePicker()
    lazy var toolbar: UIToolbar = {
        let toolBar = UIToolbar(frame: CGRect(x: 0.0,
                                              y: UIScreen.main.bounds.size.height - 300,
                                              width: UIScreen.main.bounds.size.width,
                                              height: 50))
        toolBar.barStyle = .default
        toolBar.items = [UIBarButtonItem(title: "Done",
                                         style: .done,
                                         target: self,
                                         action: #selector(onDoneButtonTapped))]

        return toolBar
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Settings"
        tableView.tableFooterView = UIView()

        datePicker.frame = CGRect(x: 0.0,
                                  y: UIScreen.main.bounds.size.height - 300,
                                  width: UIScreen.main.bounds.size.width,
                                  height: 300)
        self.datePicker.isHidden = true
        self.view.addSubview(datePicker)
        self.toolbar.isHidden = true
        self.view.addSubview(toolbar)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Row.allCases.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        let rowType = Row(rawValue: indexPath.row)
        cell.textLabel?.text = rowType?.title

        switch rowType {
        case .clearAllCache:
            cell.textLabel?.textColor = .red
        case .autoUpdateEnabled:
            let switchView = UISwitch(frame: .zero)
            switchView.setOn(ContentBlocking.instance.isAutoUpdateEnabled, animated: true)
            switchView.addTarget(self, action: #selector(self.autoUpdateSwitchChanged), for: .valueChanged)
            cell.accessoryView = switchView
        case .systemTime:
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.locale = .current

            cell.detailTextLabel?.text = dateFormatterGet.string(from: systemTime)
        default:
            break
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        switch Row(rawValue: indexPath.row) {
        case .allowedDomains:
            // Navigate to allowed domains
            navigationController?.pushViewController(AllowedDomainsViewController(), animated: true)
        case .clearAllCache:
            FileManager.default.clearCache()
            let alert = UIAlertController(title: nil, message: "All cached data is removed!", preferredStyle: .alert)
            alert.addAction(.init(title: "OK", style: .default, handler: nil))
            present(alert, animated: true)
        case .systemTime:
            datePicker.date = systemTime
            toolbar.isHidden = false
            datePicker.isHidden = false
        default:
            break
        }
    }

    @objc
    func onDoneButtonTapped() {
        self.datePicker.isHidden = true
        self.toolbar.isHidden = true

        systemTime = datePicker.date
        tableView.reloadData()
    }

    @objc
    func autoUpdateSwitchChanged(sender: UISwitch) {
        ContentBlocking.instance.setAutoUpdateStatus(isEnabled: sender.isOn)
    }
}
