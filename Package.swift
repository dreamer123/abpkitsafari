// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ABPKitSafari",
    platforms: [.iOS(.v11), .macOS(.v10_12)],
    products: [
        .library(name: "ABPKitSafari",
                 type: .dynamic,
                 targets: ["ABPKitSafari"])
    ],
    dependencies: [
        .package(name: "ABPKitCore",
                 url: "git@gitlab.com:eyeo/adblockplus/abpkitcore.git",
                 .branch("master"))
    ],
    targets: [
        .target(name: "ABPKitSafari",
                dependencies: ["ABPKitCore"],
                path: "Sources",
                exclude: ["ABPKitSafariDemo"]),
        .testTarget(name: "ABPKitSafariTests",
                    dependencies: ["ABPKitSafari", "ABPKitCore"],
                    path: "Tests")
    ]
)
