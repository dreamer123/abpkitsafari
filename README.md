# ABPKitSafari

Easy content blocking for Safari.

## Features

ABPKitSafari allows to easily bring content blocking to your safari extension app by providing the next features:

-  Choose and set a particular filter list to a particular content blocking extension. By implication you can have multiple content blocking extensions created.
-  ABPKitSafari automatically reloads the content blocking extension when a new filter list is set or when allowed domains list changed.
-  ABPKitSafari can handle the extension reload request for you.

ABPKitSafari uses ABPKitCore to load a particular filter list, thus it will incorporate all of the features provided by ABPKitCore when loading a filter list. See more [ABPKitCore](https://gitlab.com/eyeo/adblockplus/abpkitcore)

To see the full sequence of action that happens when a filter list is set check [Filter List Setting Sequence](https://gitlab.com/eyeo/adblockplus/abpkitsafari/-/wikis/Filter-list-setting-sequence-diagram)

## Usage

- Filter list setting:

```swift
import ABPKitSafari

let appInfo = App(name: "MyApp", version: "x.x.x")
let filterListSetter = ContentBlockingComposer.makeFilterListSetter(appInfo: appInfo,
                                                                    appGroupURL: myAppGroupURL,
                                                                    appBundle: myBundle)
let source: FilterListSource = // set the source here                     
filterListSetter.setFilterList(forSource: source, toContentBlocker: "myContentBlockingExtensionIdentifier") { result in
   // Handle set result
}
```

- Extension reload handling

```swift

import ABPKitSafari
import Foundation

class ContentBlockerRequestHandler: NSObject, NSExtensionRequestHandling {
    private var abpRequestHandler = ContentBlockingComposer.makeContentBlockerRequestHandler(appGroupURL: myAppGroupURL)
    
    func beginRequest(with context: NSExtensionContext) {
        abpRequestHandler.completeExtensionRequest(inContext: context,
                                                   forContentBlocker: "myContentBlockingExtensionIdentifier",
                                                   completion: { _ in })
    }
}
```
- Allowed domains

```swift
import ABPKitSafari

let allowedDomainsRepo = ContentBlockingComposer.makeAllowedDomainsRepository(appGroupURL: myAppGroupURL)

allowedDomainsRepo.addDomains(["some.com", "other.com"]) { result in 
   // handle result
}

allowedDomainsRepo.removeDomains(["some.com"]) { result in 
   // handle result
}

allowedDomainsRepo.loadDomains { result in 
  // handle result
}
```

## Documentation

For more public API information check the [documentation](https://eyeo.gitlab.io/adblockplus/abpkitsafari/index.html)

## Integration

- Swift Package Manager

ABPKitSafari is available to be used through SPM, you can add it as a dependency to your own package or application.

- Manually:

You can directly download and integrate ABPKitSafari as a package or as a framework.
